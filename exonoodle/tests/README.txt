#--------------------------------------------------------------------------#
#                            TRANSIT GENERATOR                             #
#                      TEST FUNCION - SEDfileCreation                      #
#--------------------------------------------------------------------------#
#
# CONTRUBUTORS :
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#
# Last modification : 2018/02/21
# Part of TRANSIT GENERATOR program / Test for ToolBox
# Shall be in ./XTESTS

This folder contains the test files for TransitGenerator programme.
Tests shall be launch following that sequence in bash console from the TransitGenerator folder :
> export PYTHONPATH=./
> py.test XTESTS/
