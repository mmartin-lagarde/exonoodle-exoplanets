#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                            TRANSIT GENERATOR                             #
#                      TEST FUNCION - SEDfileCreation                      #
# --------------------------------------------------------------------------#
#
# CONTRUBUTORS :
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#
# Part of TRANSIT GENERATOR program / Test for ToolBox
# Shall be in ./tests
#
#   to launch type >>> py.test test_blackbody.py


# External library imports
import numpy as np
from scipy.integrate import trapz as local_integration  # non-cumulative

# Built-in library imports
from exonoodle.utils import conservative_interpolation


def test_conservative_interpolation():
    """
    Test Blackbody

    :return:
    :rtype:
    """

    waves_in = np.arange(10, 20, 0.01)
    waves_out = np.arange(10.5, 19.5, 0.5)
    flux_in = waves_in * 0  # waves_in.copy()
    x_peak = np.arange(10) / 10. * np.pi
    size_flux = np.size(flux_in)
    flux_in[int(size_flux / 2 - 3):int(size_flux / 2 + 7)] += 4 * np.sin(x_peak)

    #  coarse grid   grid is supposed increasing
    bound_index_min_out = np.min(np.where(waves_out >= 14))
    bound_index_max_out = np.max(np.where(waves_out <= 16.5))
    # just for completeness, not used
    bound_wave_min_out = waves_out[bound_index_min_out]
    bound_wave_max_out = waves_out[bound_index_max_out]
    # print(bound_wave_min_out, bound_wave_max_out)

    #  fine grid after, using coarse grid bounds
    bound_index_min_in = np.min(np.where(waves_in >= bound_wave_min_out))
    bound_index_max_in = np.max(np.where(waves_in <= bound_wave_max_out))
    # just for completeness, not used
    bound_wave_min_in = waves_in[bound_index_min_in]
    bound_wave_max_in = waves_in[bound_index_max_in]
    # print(bound_wave_min_in , bound_wave_max_in)

    # bound_wave_min_out  = waves_out[np.min(np.where(waves_out > 14))]
    # bound_wave_max_out  = waves_out[np.max(np.where(waves_out < 16.5))]
    # bound_wave_min_in   = waves_in[np.min(np.where(waves_in > bound_wave_min_in))]
    # bound_wave_max_in   = waves_in[np.max(np.where(waves_in < bound_wave_max_in))]
    #
    # bound_index_min_out = np.min(np.where(waves_out > 14))
    # bound_index_max_out = np.max(np.where(waves_out < 16.5))
    # bound_index_min_in  = np.min(np.where(waves_in > bound_wave_min_in))
    # bound_index_max_in  = np.max(np.where(waves_in < bound_wave_max_in))

    flux_out = conservative_interpolation(waves_in, flux_in, waves_out)

    integral_in = local_integration(flux_in[bound_index_min_in:bound_index_max_in],
                                    waves_in[bound_index_min_in:bound_index_max_in])
    integral_out = local_integration(flux_out[bound_index_min_out:bound_index_max_out],
                                     waves_out[bound_index_min_out:bound_index_max_out])

    # plt.plot(waves_in, flux_in, 'r')
    # plt.plot(waves_out, flux_out, 'g--+')
    # plt.plot(waves_out, flux_interp1d, 'b--+')
    # plt.show()

    return
