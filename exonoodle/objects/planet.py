#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                    exoNoodle : Instantaneous spectrum                     #
#                           -- Planet settings --                           #
# --------------------------------------------------------------------------#
#
#
# CONTRUBUTORS :
# --------------
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#
# Part of exoNoodle package
# Main : system_instant_spectrum() in compute.py
# (TBI : To be integrated)
# All units are astropy.units
#
# Developped in Python 3
# Suitable for Python 2.7
#
#
#  CONTAINS :
# -----------
#   class PlanetContribution
#        init, emissionDay, emissionNight, reflection, transmission,
#        str, repr

from .. import utils
import numpy as np

import logging
LOG = logging.getLogger('exonoodle.planet')

## RG version Jeu 23 jui 2022 conservative_interpolation for transmission
__ma_version='June 2022'

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# .___  .                         .          ___                 .            _             .
# /   \ |     ___  , __     ___  _/_       .'   \   __.  , __   _/_   .___  ` \ ___  ,   . _/_   `   __.  , __
# |,_-' |    /   ` |'  `. .'   `  |        |      .'   \ |'  `.  |    /     | |/   \ |   |  |    | .'   \ |'  `.
# |     |   |    | |    | |----'  |        |      |    | |    |  |    |     | |    ` |   |  |    | |    | |    |
# /    /\__ `.__/| /    | `.___,  \__/      `.__,  `._.' /    |  \__/ /     / `___,' `._/|  \__/ /  `._.' /    |
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

# Class describing the planet transmission contribution.
#
#
# Reads the entry files : configuration.ini
#                         StarSpectrum|.txt
#                                     |.dat
#                                     |.fits


class PlanetContribution(object):
    """
    Class taking into account the contribution fo the planet to the emission of the system. Either emission or
    absorption features.

    . Developped for transitGenerator project
    (Creation 2018/01/05 (MML) -- Last update 2018/01/25 (MML))

    """

    def __init__(self, configuration, wavelength, key):
        """
        . Initialises a PlanetContribution class object.

        . Developped for transitGenerator project
        (Creation 2017/12/18 (MML) -- Last update 2018/02/13 (RG))


        >>>> PARAMETERS
        • configuration : dictionnary containing all the configuration of the system
        • wavelength    : quantity array - wavelength array
        • key           : string - to recognise which planet to pick in dictionnary
        • [verbose]     : Boolean - To enable prints [optional][default=False]

        >>>> RETURNS
        /no returns/
        """
        LOG.debug('>> Planet Creation')
        self.wavelength = wavelength
        
        planet_config = configuration["planet"]
        self.name = planet_config["name"]
        LOG.debug('        > Planet {} as {}'.format(self.name, key[:-1]))
        self.mass = planet_config["mass"]
        self.radius = planet_config["radius"]
        self.phaseShift = planet_config["phase_shift"]
        
        self.dayTemperature = planet_config["day_temperature"]
        self.nightTemperature = planet_config["night_temperature"]
        self.dayEmissionFile = planet_config["day_emission_file"]
        self.nightEmissionFile = planet_config["night_emission_file"]
        self.absorptionFile = planet_config["absorption_file"]
        self.albedo = planet_config["albedo"]
        self.albedoFile = planet_config["albedo_file"]

        orbit_config = configuration["orbit"]
        self.orbitalPeriod = orbit_config["orbital_period"]
        self.orbitalInclination = orbit_config["inclination"]
        self.semiMajorAxis = orbit_config["semi_major_axis"]
        # self.t0Transit = orbit_config["T0_transit"]
        # self.t0Occultation = orbit_config["T0_occultation"]

        #
        # self.reflection(self) # does not work , verbose=verbose)

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def emissionDay(self, distance, verbose=False):
        """
        . Module to create the spectrum of the day side of the planet, by calling the
        utils.readSpectrum() function.

        . Sets up the self.dayEmissionFlux parameter, later used by SystemContribution object (with the
        orbitalPhase as coefficient)

        . Developped for transitGenerator project
        (Creation 2018/01/05 (MML) -- Last update 2018/02/13 (RG))


        >>>> PARAMETERS
            • distance      : quantity - distance from Earth to star
            • [verbose]     : Boolean - To enable prints [optional][default=False]


        >>>> RETURNS
            /no returns/
        """

        self.dayEmissionFlux, wavelength, data_name = utils.readSpectrum(self.wavelength, fileName=self.dayEmissionFile,
                                                    temperature=self.dayTemperature, radius=self.radius,
                                                    distance=distance,
                                                    verbose=verbose)
        if wavelength[0] != self.wavelength[0]:
            LOG.warning("           ---------> Warning :: Something went wrong in the cooking of your day emission flux")

        return data_name

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def emissionNight(self, distance, verbose=False):
        """
        . Module to create the spectrum of the night side of the planet, by calling the
        utils.readSpectrum() function.

        . Sets up the self.nightEmissionFlux parameter, later used by SystemContribution object (with the
        orbitalPhase as coefficient)

        . Developped for transitGenerator project
        (Creation 2018/01/05 (MML) -- Last update 2018/02/13 (RG))


        >>>> PARAMETERS
            • distance      : quantity - distance from Earth to the host star
            • [verbose]     : Boolean - To enable prints [optional][default=False]


        >>>> RETURNS
            /no returns/
        """

        self.nightEmissionFlux, wavelength, data_name = utils.readSpectrum(self.wavelength, fileName=self.nightEmissionFile,
                                                      temperature=self.nightTemperature, radius=self.radius,
                                                      distance=distance, verbose=verbose)

        if wavelength[0] != self.wavelength[0]:
            print("           ---------> Warning :: Something went wrong in the cooking of your day emission flux")

        return data_name

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def reflection(self, verbose=False):
        """
        . Module to create a table albedo versus wavelength or just a scalar, depending on the mention of a file
        or just a tmp.

        . Sets up the self.albedo parameter

        . Developped for transitGenerator project
        (Creation 2018/01/09 (MML) -- Last update 2018/02/14 (MML))


        >>>> PARAMETERS
            • wl                    : 1D np.array - wavelengths in [m]
            • fileName              : string - complete name of the configuration file


        >>>> RETURNS
            /No returns/
        """
        if self.albedoFile != "none":
            albedo = utils.WavelengthDepdtVariable(fileName=self.albedoFile)
            LOG.debug('           . Reading file: {}'.format(self.albedoFile))
            self.albedo = albedo.getInterpolated(self.wavelength)

        self.dilutionFactor = (self.radius / (2 * self.semiMajorAxis)) ** 2

        return

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def transmission(self, starRadius, verbose=False):
        """
        . Module to create the transmission spectrum of the planet, either by reading
        it in a separate file, or by setting a constant radius if no absorption file
        is mentionned in the configuration file.

        . Absorption is defined as (Rp/R*)**2

        . Sets up the self.absorption parameter
        . Warning :: self.radius can be upgraded from scalar to vector

        . Developped for transitGenerator project
        (Creation 2018/01/05 (MML) -- Last update 2018/02/13 (RG))


        >>>> PARAMETERS
            • starRadius    : quantity - radius of the host star
            • [verbose]     : Boolean - To enable prints [optional][default=False]


        >>>> RETURNS
            /No returns/
        """
        if self.absorptionFile != "none":
            absorption = utils.WavelengthDepdtVariable(fileName=self.absorptionFile)
            LOG.debug('           . Reading file: {}'.format(self.absorptionFile))
            ## RG version Jeu 23 jui 2022
            ## beware getInterpolated extrapolate, but conservative_interpolation does not extrapolate
            #self.absorption = absorption.getInterpolated(self.wavelength)
            self.absorption = utils.conservative_interpolation(absorption.wavelength.value, absorption.data.value, self.wavelength.value)

        else:
            # non keep it scalar radii = self.radius.repeat(self.wavelength.size)
            self.absorption = (self.radius.repeat(self.wavelength.size) / starRadius) ** 2

        self.radius = np.sqrt(self.absorption) * starRadius
        return

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def __str__(self):
        """
            when you type >> print(my_object)
            __str__ goal is to be readable
        """
        sb = []
        for key in self.__dict__:
            sb.append("   • {key}='{value}'".format(key=key, value=self.__dict__[key]))
        return '\n'.join(sb)

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def __repr__(self):
        """
            when you just type >> my_object
            __repr__ goal is to be unambiguous
        """
        # return self.__str__()
        return "%s(%r)" % (self.__class__, self.__dict__)
