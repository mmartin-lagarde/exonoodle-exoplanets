#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                            TRANSIT GENERATOR                             #
#                      TEST FUNCION - SEDfileCreation                      #
# --------------------------------------------------------------------------#
#
# CONTRUBUTORS :
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#
# Part of TRANSIT GENERATOR program / Test for ToolBox
# Shall be in ./tests
#
#   to launch type >>> py.test test_blackbody.py


# External library imports
import os
from astropy import units as u
import numpy.testing as npt
import numpy as np

# Built-in library imports
from exonoodle.objects import StarContribution
from exonoodle import config as c

TEST_DIR = os.path.dirname(__file__)


def test_StarContribution():
    """
    Test Star Contribution

    :return:
    :rtype:
    """
    waves = np.array([1., 10.]) * u.micron

    ref_flux = np.array([3.78308961, 0.19028006]) * u.mJy

    config = c.read(os.path.join(TEST_DIR, 'Sun1000Characteristics.txt'))
    my_star = StarContribution(config, waves, verbose=True)
    #  check that the star has read the configuration
    # assert(my_star.spectralFile=='Sun1000.fits')
    assert (my_star.spectralFile == 'none')
    assert (my_star.spectralType == 'G')
    np.testing.assert_allclose(my_star.distance.to(u.pc).value, 1000.)

    np.testing.assert_allclose(my_star.radius.to(u.solRad).value, 1.)

    np.testing.assert_allclose(my_star.radius.to(u.solRad).value, 1., rtol=1E-15)

    npt.assert_array_equal(waves.value, my_star.wavelength.value)
    # tmp is recomputed, and reference have only 3 decimals
    #  check the tmp see blackbdoy
    np.testing.assert_allclose(ref_flux.value, my_star.flux.value, rtol=1.1E-6)

