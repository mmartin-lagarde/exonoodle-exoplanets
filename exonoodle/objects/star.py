#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                    exoNoodle : Instantaneous spectrum                     #
#                            -- Star settings --                            #
# --------------------------------------------------------------------------#
#
#
# CONTRUBUTORS :
# --------------
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#   - (CC)  : Christophe Cossou (IAS-Orsay)
#             christophe.cossou@ias.u-psud.fr
#
# Part of exoNoodle package
# Main : system_instant_spectrum() in compute.py
# (TBI : To be integrated)
# All units are astropy.units
#
# Developped in Python 3
# Suitable for Python 2.7
#
#
#  CONTAINS :
# -----------
#   class StarContribution
#        init, str, repr
#        class LimbDarkeningContribution
#               init, getLimbDarkening, compute

from .. import utils
import numpy as np
import os
from pdb import set_trace as stop
from astropy.io import ascii
from astropy import units as u

import logging
LOG = logging.getLogger('exonoodle.star')

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#         _____  .                        ___                 .            _             .
#        (      _/_     ___  .___       .'   \   __.  , __   _/_   .___  ` \ ___  ,   . _/_   `   __.  , __
#         `--.   |     /   ` /          |      .'   \ |'  `.  |    /     | |/   \ |   |  |    | .'   \ |'  `.
#            |   |    |    | |          |      |    | |    |  |    |     | |    ` |   |  |    | |    | |    |
#       \___.'   \__/ `.__/| /           `.__,  `._.' /    |  \__/ /     / `___,' `._/|  \__/ /  `._.' /    |
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# Class describing the star contribution.
#
#
# Reads the entry files : configuration.ini
#                         StarSpectrum|.txt
#                                     |.dat
#                                     |.fits
class StarContribution(object):
    """
    Class taking into account the contribution fo the star to the emission of the system.

    . Developped for exoNoodle project
    (Creation 2017/12/18 (MML) -- Last update 2018/06/11 (CC))


    :Parameters:
    filename: string, the full filename of the configuration file.
    wavelength: array or scalar, quantity
    verbose : logical, keyed parameter, default tmp False

    """

    def __init__(self, configuration, wavelength=None, verbose=False):
        """
        . Initialises a StarContribution class object.

        . Developped for exoNoodle project
        (Creation 2017/12/18 (MML) -- Last update 2018/06/11 (CC))


        :param dict configuration: containing all the configuration of the system
        :param nd.array wavelength: wavelength (quantity array)
        :param bool verbose: [optional] To enable prints [optional][default=False]

        """
        LOG.debug('>> Star Creation')

        star_config = configuration["star"]

        self.name = star_config["name"]
        LOG.debug('        > Star {}'.format(self.name))
        self.mass = star_config["mass"]
        self.radius = star_config["radius"]
        self.temperature = star_config["temperature"]
        self.distance = star_config["distance"]
        self.spectralType = star_config["spectral_type"]
        self.spectralFile = star_config["spectral_file"]
        self.flux, self.wavelength, data_name_trash = utils.readSpectrum(wavelength, fileName=self.spectralFile, temperature=self.temperature,
                                         radius=self.radius, distance=self.distance, verbose=verbose)

        if star_config["LD_method"] == 'none':
            self.LimbDarkening = None

        else:
            self.LimbDarkening = LimbDarkeningContribution(configuration, self.wavelength)
        # if verbose: print(self.data)

    def __str__(self):
        """
            when you type >> print(my_object)
            __str__ goal is to be readable
        """

        sb = []
        for key in self.__dict__:
            sb.append("   • {key}='{value}'".format(key=key, value=self.__dict__[key]))

        return '\n'.join(sb)


    def __repr__(self):
        """
            when you just type >> my_object
            __repr__ goal is to be unambiguous
        """
        # return self.__str__()
        return "%s(%r)" % (self.__class__, self.__dict__)

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
class LimbDarkeningContribution(object):


    def __init__(self, dictionary, wavelength):
        """
        . Initialises a LimbDarkeningContribution class object.

        . Developped for exoNoodle project
        (Creation 2017/12/18 (MML) -- Last update 2018/06/11 (CC))


        :param dict dictionnary: containing all the configuration of the system
        :param nd.array wavelength: wavelength (quantity array)
        :param bool verbose: [optional] To enable prints [optional][default=False]

        """
        self.configuration = dictionary
        self.wavelength = wavelength

        self.METHOD_AVAILABLE = {
            "quadratic": self.__quadratic,
            "claret4": self.__claret4
        }

        star_config = dictionary["star"]

        self.method = star_config["LD_method"]

        f = star_config["LD_file"]
        c = star_config["LD_coeff"]
        if f != "none":
            self.file = star_config["LD_file"]
            self.data = self.__read_file(self.file)
        elif c != [0.]:
            ld_coeff = np.array(star_config["LD_coeff"])
            self.data = np.broadcast_to(ld_coeff, (self.wavelength.size, ld_coeff.size))
        else:
            raise ValueError("Missing either LD_file or LD_coeff parameter in config file")

        self.compute = self.METHOD_AVAILABLE[self.method]
        #ascii.write(self.data, "extrapolated_LDCs.txt", overwrite=True) #Create a file with extrapolated LimbDarkening coefficients.

    def __quadratic(self, gamma, selectionOK='all'):

        # - - - - - - - - - - - - - - - - - -  SELECTING THE GOOD PORTIONS OF VECTOR - - - - - - - - - - - - - - -
        if isinstance(selectionOK, str) and selectionOK == "all":
            selectionOK = np.arange(len(self.wavelength))

        # - - - - - - - - - - - - - - - - - - - - - -  QUADRATIC EXPRESSION  - - - - - - - - - - - - - - - - - - -
        light_ratio = 1. - self.data[selectionOK, 0] * (1. - np.cos(gamma)) \
                         - self.data[selectionOK, 1] * (1. - np.cos(gamma)) ** 2

        # Renormalisation ot I_0 (Howarth+ 2011)
        ldc_area = 2. * (1./2. - self.data[selectionOK, 0]/6 - self.data[selectionOK, 1]/12)

        return light_ratio/ldc_area

    def __claret4(self, gamma, selectionOK='all'):

        # - - - - - - - - - - - - - - - - - -  SELECTING THE GOOD PORTIONS OF VECTOR - - - - - - - - - - - - - - -
        if isinstance(selectionOK, str) and selectionOK == "all":
            selectionOK = np.arange(len(self.wavelength))

        # - - - - - - - - - - - - - - - - - - - - - -  CLARET 4 EXPRESSION - - - - - - - - - - - - - - - - - - - -
        light_ratio = 1. - self.data[selectionOK, 0] * (1. - np.cos(gamma)**(1./2.))  \
                         - self.data[selectionOK, 1] * (1. - np.cos(gamma))           \
                         - self.data[selectionOK, 2] * (1. - np.cos(gamma)**(3./2.))  \
                         - self.data[selectionOK, 3] * (1. - np.cos(gamma)**2.)

        # Renormalisation ot I_0 (Howarth+ 2011)
        ldc_area = 2.* (1./2. - (self.data[selectionOK, 0]/10 \
                            + self.data[selectionOK, 1]/6 \
                            + (3*self.data[selectionOK, 2])/14 \
                            + self.data[selectionOK, 3]/4))
        #print("LDCarea:", ldc_area)

        return light_ratio/ldc_area


    def __read_file(self, filename):
        """
        Used only in __init__ to read the data file and return the limb darkening coefficient in a numpy array

        :param string filename:
        :return:
        :rtype:
        """

        datafile = ascii.read(filename)

        # Wavelength associated with the original file
        in_wave = datafile["wavelength"].data * u.Unit(datafile["wavelength"].unit)

        columns = datafile.colnames

        columns.remove("wavelength")
        columns.sort()

        nb_coeff = len(columns)
        nb_waves = len(self.wavelength)

        data = np.zeros((nb_waves, nb_coeff))

        for (id, col) in enumerate(columns):
            coeff = datafile[col].data * u.Unit(u.dimensionless_unscaled)

            tmp_data = utils.WavelengthDepdtVariable(data=coeff, wavelength=in_wave)

            data[:, id] = tmp_data.getInterpolated(wavelength=self.wavelength)

        return data
