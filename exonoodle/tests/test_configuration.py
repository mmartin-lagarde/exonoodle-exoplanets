#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Marine Martin-Lagarde (CEA-Saclay)  marine.martin-lagarde@cea.fr
#    René Gastaud (CEA-Saclay)          rene.gastaug@cea.fr
#    4 February 2018
#
#   To launch type >>> py.test test_configuration.py
#   Create the file xx_config.txt
#
# External library imports
import os
from astropy import units as u

import exonoodle.utils

TEST_DIR = os.path.dirname(__file__)


def test_configuration():
    """
    Test read configuration

    :return:
    :rtype:
    """
    config = exonoodle.config.read(os.path.join(TEST_DIR, 'Sun1000Characteristics.txt'))

    starName = config["star"]["name"]
    assert (starName == 'Sun1000pc')

    starSpectralType = config["star"]["spectral_type"]
    assert (starSpectralType == 'G')

    starSpectralFile = config["star"]["spectral_file"]
    assert (starSpectralFile == 'none')

    starRadius = config["star"]["radius"]
    assert ((1 * u.solRad).decompose().value == starRadius.value)

    starMass = config["star"]["mass"]
    assert ((1 * u.solMass).decompose().value == starMass.value)

    starTemperature = config["star"]["temperature"]
    assert ((5000. * u.K).value == starTemperature.value)

    starDistance = config["star"]["distance"]
    assert ((1000 * u.pc).decompose().value == starDistance.value)


