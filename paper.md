---
title: 'Exonoodle: Synthetic time-series spectra generator for transiting exoplanets'
tags:
  - Python
  - astronomy
  - exoplanets
  - modelling
  - spectral
authors:
  - name: Marine Martin-Lagarde
    orcid: 0000-0003-0523-7683
    affiliation: 1
  - name: Christophe Cossou
    orcid: 0000-0001-5350-4796
    affiliation: "1, 2"
  - name: René Gastaud
    affiliation: 1
affiliations:
 - name: AIM, CEA, CNRS, Université Paris-Saclay, Université de Paris, Sorbonne Paris Cité, F-91191 Gif-sur-Yvette, France
   index: 1
 - name: Institut d'Astrophysique Spatiale, CNRS, Université Paris-Sud, Université Paris-Saclay, Bât. 121, 91405, Orsay Cedex, France
   index: 2
date: 4 April 2020
bibliography: paper.bib
aas-doi: 10.3847/1538-3881/abac09
aas-journal: The Astronomical Journal
---

# Summary

`exonoodle` is a Python 3 package which generates time-series spectra for transiting 
exoplanet systems. The package reads modelled spectral contributions for a given planet and
star (or generates a black-body spectrum) and creates the expected spectral variation of the
star-planet system as the planet is orbiting around the star.

The details of the package algorithm along with the scientific background can be found in a 
companion paper [@Martin-Lagarde:2020] including example use cases. Technical usage and
a detailed user manual are available on GitLab.

The main calculation of the transit spectral light-curve is adapted from the model of
[@Mandel:2002]. The novelty of this code compared to those that are already available
to the community (see Table. 1) is that it includes the planetary contribution, and
computes the spectra directly. This means the algorithm solutions are different compared
to existing codes, because it does not directly provide light-curves, it provides the
actual time-series spectral data. Hence, the computation is almost fully vectorial in
wavelength, and the output is a folder containing a set of spectral  files. Flexibility
and efficiency in the input of the package is due to extensive use of  the `Astropy`
package [@astropy] (and especially `astropy.units`). Computing the complete light curve
in parallel, using 4 CPU on a laptop (Intel i7-6820HQ CPU @ 2.70GHz) take approximately
~5 minutes and scale with the number of CPUs used and their individual specifications.

The `exonoodle` package has been developed in the framework of preparations for the James Webb Space
Telescopes Early Release Science (ERS)  observations, and is already used along with the MIRI
instrument simulator to produce synthetic MIRI Low Resolution Spectroscopic data. These data will
be used in ERS data challenges planned ahead of launch in late 2020.

# Acknowledgments
This development has received funding from the European Union’s Horizon 2020 Research 
and Innovation program, under Grant Agreement 776403, and the LabEx P2IO, the French 
ANR contract 05-BLANNT09-573739. Marine Martin-Lagarde is partly funded with a CNES 
scholarship.