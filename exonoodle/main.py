from . import config
from . import parallel_lightcurve
from . import sequential_lightcurve
from . import single_spectrum

import logging
LOG = logging.getLogger(__name__)


def run(filename):
    """
    Depending on the configuration file, will run a single spectrum, sequential light curve or parallel light curve

    :param str filename: .ini configuration file for exonoodle
    """
    configuration = config.read(filename, skip_warnings=True)

    soft_config = configuration["exonoodle"]

    nb_proc = soft_config["nb_proc"]
    sampling = soft_config["time_sampling"]

    if sampling == 0:
        single_spectrum.single_spectrum(filename)
    else:
        if nb_proc == 0:
            sequential_lightcurve.sequential_lightcurve(filename)
        else:
            parallel_lightcurve.parallel_lightcurve(filename)
