#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                    exoNoodle : Instantaneous spectrum                     #
# --------------------------------------------------------------------------#
#                      ***DEVELOPMENT IN PROGRESS***
#
# CONTRUBUTORS :
# --------------
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#   - (CC)  : Christophe Cossou (IAS-Orsay)
#             christophe.cossou@ias.u-psud.fr
#
# GOAL :
# ------
# Aims to generate a variable spectral source integrating the whole transit
# curve of an exoplanetary system, in JWST-MIRI spectral range. Generation
# of signal inputs compatible with MIRICLE simulations as variable source.
#
#
# DEPENDANCIES :
# --------------
# This packages goes along with module files :
#      - utils.py
#      - transit_objects.py
#
# Several entry files are necessary :
#      - configuration.ini (At least)
#     (+) Whatever is included in this file as source file
#         for the contributions
#
#
#  CONTAINS :
# -----------
#   functions:
#       init_system
#       phase_system_spectrum
#
# Developped in Python 3
# Suitable for Python 2.7
# The Jupyter Notebook no longer available. **Work in progress**

from __future__ import print_function

import datetime
import os

today=datetime.date.today()

# IMPORT MODULES
from . import utils
from . import objects
from . import config

import logging
LOG = logging.getLogger('exonoodle.compute')

def init_system(wl, params):
    """

    :param wav: wavelength sample
    :type wav: nd.array (quantities)
    :param params: input filename parameter file (.txt) or config object
    :type params: str or config.

    :return: system object initialized
    :rtype: objects.SystemContribution
    """

    # >>>>>>>>>>> RETRIEVAL OF THE CONFIGURATION
    # configuration = utils.readConfiguration(config_filename)
    if isinstance(params, str):
        configuration = config.read(params)
    else:
        configuration = params

    # >>>>>>>>>>> CREATION OF THE SYSTEM
    star = objects.StarContribution(configuration, wl)

    planet = objects.PlanetContribution(configuration, wl, 'Planet1_')
    LOG.debug("         • Day Emission")

    planet_format = planet.emissionDay(star.distance)

    if planet_format == 'flux_ratio':
        planet.dayEmissionFlux = planet.dayEmissionFlux * star.flux

    LOG.debug("         • Night Emission")
    planet_format = planet.emissionNight(star.distance)
    if planet_format == 'flux_ratio':
        planet.nightEmissionFlux = planet.nightEmissionFlux * star.flux
    LOG.debug("         • Reflection")
    planet.reflection()
    LOG.debug("         • Transmission")
    planet.transmission(star.radius)

    system = objects.SystemContribution(star, planet)

    return system



def phase_system_spectrum(time, params,
                          output_dir, start_index, padding, wavelength):
    """
    Will return a mimicked spectrum taking into account the geometry
    and phase of the whole system, with the input of a planetary system and spectrum for each objet

    Compute spectrum for a set of time. If only one is given, then it's a simple curve

    :param list time: list of floats
    :param str params: input filename parameter file (.txt) or configuration object
    :param numpy.ndarray wav: wavelength [micron] at which we want a lightcurve
    :param str output_dir: folder in which are stored the sed files
    :param int padding: Number of trailer zeros in SED filename to ensure the SED files are correctly sorted
    :param int start_index: start index for SED numbering scheme.
    :param wavelength: Wavelength sampling (quantity array)
    :type wavelength: np.array(float) (quantity)

    :return: fluxes [milliJansky] at 5, 10 and 15 microns (file creation)
    :rtype: 0
    """

    SED_prefix = "SED"
    system = init_system(wavelength, params)

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    #                                           CALL THE CALCULATION ROUTINE
    # Loop on times
    sed_index = start_index
    for ti in time:
        # utils.progressBar((phase[i].tmp - time_min), (time_max - time_min), 'Cooking noodles... ')
        # >>>>>>>>>>> CREATION OF THE SYSTEM
        spectrum = system.spectrum(ti)

        # >>>>>>>>>>> FILE CREATION MODULES.
        SED_output = "{}_{:0{}d}.dat".format(SED_prefix, sed_index, padding)
        utils.SEDfileCreation(wavelength, spectrum, os.path.join(output_dir, SED_output))  # ,verbose)
        sed_index += 1


    return 0
