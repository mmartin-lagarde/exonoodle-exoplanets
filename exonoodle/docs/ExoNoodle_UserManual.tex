\documentclass{article}
\usepackage[square, numbers]{natbib}
\usepackage[french, english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[pdftex]{graphicx}
\usepackage{charter}
\usepackage{array}
\usepackage{wrapfig}
%\usepackage[a4paper]{geometry}
\usepackage{url}
\usepackage[bottom]{footmisc}
\usepackage{color}
\usepackage{colortbl}
\usepackage{hhline}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[hidelinks]{hyperref}
\usepackage[printonlyused, withpage]{acronym}
\usepackage{subfig}
\usepackage{mathrsfs}
\usepackage{stix}
\usepackage{fontawesome}
\usepackage{pifont}
\usepackage{float}
\usepackage{outlines}
\usepackage[dvipsnames]{xcolor}
\usepackage{framed}
\usepackage{fancybox}
\usepackage{listings}

\definecolor{lightblue}{HTML}{C7D7E1}

 \newenvironment{leftbarshaded}{%
  \def\FrameCommand{\vrule width 0.4mm \colorbox{lightblue}}%
  \MakeFramed {\advance\hsize-\width \FrameRestore}}%
 {\endMakeFramed}

\newenvironment{note}{\begin{leftbarshaded}\noindent\textbf{Important note} : }{\end{leftbarshaded}}

\newcommand{\highlight}[1]{\colorbox{yellow}{#1}}


\lstdefinestyle{custompy}{frame=l,
  language=Python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{ForestGreen},
  keywordstyle=\color{blue},
  commentstyle=\color{Gray},
  stringstyle=\color{ForestGreen},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\lstdefinelanguage{config} {
morekeywords={a, nb_proc, folder, time, time_sampling, t_min, t_max, wave_min, wave_max, dwave, wave_unit, name, spectral_type, spectral_file, mass, radius, temperature, distance, LD_file, LD_coeff, LD_method, absorption_file, day_temperature, day_emission_file, night_temperature, night_emission_file, phase_shift, albedo, albedo_file, mass_unit, radius_unit, temperature_unit, distance_unit, phase_shift_unit, orbital_period, inclination, impact_factor, semi_major_axis, orbital_period_unit, inclination_unit, semi_major_axis_unit, time_sampling_unit, b}
sensitive=false
morecomment=[l]{\#},
morecomment=[s]{[}{]},
morestring=[b]",
}


\lstdefinestyle{custompyconfig}{frame=single,
  language=config,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{ForestGreen},
  keywordstyle=\color{RoyalBlue},
  commentstyle=\color{Gray},
  stringstyle=\color{ForestGreen},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}


\lstdefinestyle{custombash}{frame=l,
  language=bash,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{green},
  keywordstyle=\color{blue},
  commentstyle=\color{lightgrey},
  stringstyle=\color{green},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

%%%%%%%%%%%%%%%%%%%%%%%%CONFIGURATION PAGE%%%%%%%%%%%%%%%%%%%%%%%%%%
%\usepackage{layout}
%	\selectlanguage{english}
%	\setlength{\hoffset}{0cm}
%	\setlength{\voffset}{-0.7in}
%	\setlength{\oddsidemargin}{0cm}
%	\setlength{\topmargin}{0cm}
%	\setlength{\headheight}{1cm}
%	\setlength{\headsep}{1cm}
%	\setlength{\textheight}{23.7cm}
%	\setlength{\textwidth}{15.92cm}
%	\setlength{\marginparsep}{0cm}
%	\setlength{\marginparwidth}{0cm}
%	\setlength{\footskip}{2cm}

\usepackage[pdftex]{geometry}
\geometry{margin=2.5cm}%la redéfinition des marges doit se faire avant l'appel de fancyhdr. Sinon, les entêtes et pieds de pages auront les anciennes marges.

\usepackage{fancyhdr}
	\pagestyle{fancy}
	\fancyhf{} %Vire les titre de chapitre/section du header
	\fancyfoot[L]{\includegraphics[height=0.5cm]{figures/logoCNES.pdf}~~~~\includegraphics[height=0.5cm]{figures/logoCEA.pdf}~~~~\includegraphics[height=0.4cm]{figures/logoExoplANETS-A.pdf}}
	\fancyfoot[C]{}
	\fancyfoot[R]{\thepage}
	\fancyhead[C]{\footnotesize{\textsc{User Manual exoNoodle} ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Martin-Lagarde ~~~ 2020}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%TITLE%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{\textsc{exoNoodle - User Manual} \\ \hrulefill}
\author{Marine \textsc{Martin-Lagarde}\\ Christophe Cossou (minor changes)}
\date{\small{\textsc{June 2023 \ding{167} V1.0}}}

\selectlanguage{english}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%% DEBUT DU DOCUMENT %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\maketitle

\begin{figure}[h!]
\begin{center}
\includegraphics[height=4cm]{figures/exoNoodle.pdf}
\label{logo}
\end{center}
\end{figure}

\paragraph{} \texttt{exonoodle} is a package that generates spectral time-series data for a transiting exoplanetary system, which cover all or a portion of the lightcurve. The goal was to create a versatile, modular and robust code. In the frame of a complete simulator of satellite synthetic data, the goals are to highlight and quantify the influence of various effects and their contributions. We wanted to achieve this within a reasonable processing time (fractions of a second on an average personal computer for a single run). However, a complete and representative light curve takes longer. Therefore, parallelization options are available within the code and recomended. The package is suitable for any spectroscopic instrument with compatible inputs (wavelength range and resolution); but was developed for the preparation of JWST/MIRI data processing, for observations of time-series in LRS-Slitless (Low Resolution Spectrometer).
\tableofcontents
\vspace*{2cm}
\newpage

\section{\textsc{Description}}
\subsection{\textsc{Purpose}}
\paragraph{} Exonoodle is designed to create synthetic spectra of a transiting exoplanet system (star+planet). As a special case we can also generate a single spectrum for a specific orbital phase of the planet.

These spectra can then be used as input for any instrument simulator, e.g. \textit{MIRISim} in the example shown here (the one for which the code was originally designed).

Therefore, the output of the code is a file (or a series of files) used as the SED\footnote{SED: Spectral Energy Distribution} in the field of view of the simulated instrument. Hereafter, the instrument properties or noise are not considered. The goal is to describe the exoplanet system as accurately as possible up to the instrument sensitivity limit ($\approx 10$ $ppm$).

\begin{note}
The output is the spectrum of the complete system (star+planet), i.e what is seen in the line of sight at a given time.
\end{note}

\paragraph{\textsc{Contributions}} In its current state, the software takes into account the following contributions and effects to the system's spectrum:
\begin{itemize}
\item[•] The star emission spectrum;
\item[•] The effect of star limb-darkening on the transit shape;
\item[•] The system occultations: transit and eclipse;
\item[•] The planet's emission: Day and night contrast and an eventual phaseshift due to atmospherical circulation;
\item[•] The planet's reflection: Albedo integrated to the day face;
\item[•] The planet's transmission: As a variable $R_{p}\left(\lambda\right)$.
\end{itemize}
\paragraph{} All these contributions can be wavelength-dependant. Assuming you give the matching wavelength range as an input, it will work at any wavelength range and resolution. \texttt{exonoodle} also accounts for the light-travel delay on the Eclipse ephemerids and phase-curve distortion.
\subsection{\textsc{Limitations}}
\paragraph{\textsc{Assumptions}} The details of the calculations are all in the 2nd Thesis Report (03/2018) [in French - Attached to the package] or the companion paper\footnote{Martin-Lagarde et al., (2021). Exonoodle: Synthetic time-series spectra generator for transiting exoplanets. Journal of Open Source Software, 6(57), 2287, https://doi.org/10.21105/joss.02287}. The assumptions made in the calculations are the following:
\begin{itemize}
\item[•] Sharp terminator: Sharp limit between day and night contributions, considered as two homogeneous half-spheres;
\item[•] Large distances: The radius of the planet is consequently smaller than the radius of the star, which is negligible regarding the star/planet distance. ($R_{p} \ll R_{\bigstar} \ll d_{p-\bigstar}$) This allows one to consider all the light rays parallel;
\item[•] Single planet system: The system contains only one planet;
\item[•] Locked planet: The planet is 1:1 tidally-locked and on a circular orbit;
\item[•] Black-body: In the first approximation, all the emitting bodies can be considered as black-bodies. This is the default value if no file is mentioned;
\item[•] Homogeneous fading of the planet: During eclipse ingress and egress, the fading of the planet due to partial occultation by the star is considered homogeneous on day and night side ($\Lambda_\text{day} = \Lambda_\text{night}$ in Martin-Lagarde et al. 2020).
\end{itemize}

\paragraph{\textsc{Effects ignored}} The following effects are known, but not yet taken into account. Some of them might be integrated into the software in future versions \textbf{[TBI]}, others are suspected to be below the sensitivity \textbf{[BS]} of the instrument and not likely to be implemented:
\begin{itemize}
\item[•] \textbf{[TBI]} Differential fading of the planet during eclipse ingress and egress;
\item[•] \textbf{[TBI]} Additional harmonic orders in the planet light curve (other than a simple shift, e.g. 2D contributions);
\item[•] \textbf{[TBI]} Having a de-phaselock of the planet;
\item[•] \textbf{[TBI]} Having an orbital eccentricity;
\item[•] \textbf{[TBI]} More than one planet in the system;
\item[•] \textbf{[TBI]} Doppler shifts due do the planets radial velocity and Rossiter-McLaughlin effect;
\item[•] \textbf{[BS]} The terminator shift on the planet (if the parallel ray assumption is not applicable);
\item[•] \textbf{[BS]} The refractions effects due to the atmosphere (typical amplitude of 10ppm and highly degenerated according to Alp \& Demory (2018)).
\end{itemize}



\subsection{\textsc{Usage}}
\paragraph{} The folder you have downloaded contains the package and several other elements. It should contain:
\begin{itemize}
\item[-] \texttt{exonoodle/} (folder) - Containing the package itself;
\item[-] \texttt{README.adoc} - text file containing the instruction on installation, basic use, and reference for the package;
\item[-] \texttt{requirements.txt} - text file containing the basic pyhton configuration (dependencies) to run the package;
\item[-] \texttt{run\_exonoodle.py} - exec. file containing an exemple script to use the package
\item[-] \texttt{setup.py} - text file used for package installations;
\item[-] \texttt{configuration.ini} - example of configuration file. This file contains all the available parameters. All the optional ones are commented. You can find a full description later in this document.
\end{itemize}


\begin{note}
All the installations and quick start instructions are available in the \texttt{README.adoc}. It is important to follow them to ensure a smooth start. \ovalbox{\href{https://gitlab.com/mmartin-lagarde/exonoodle-exoplanets/-/blob/master/README.adoc}{Click here}}
\end{note}

After installation, a command is available to launch a computation with the file \texttt{configuration.ini}. In a terminal, you can type:
\lstset{style=custombash}
\begin{lstlisting}
exonoodle configuration.ini
\end{lstlisting}

Alternatively, in a python shell, you can type the following:
\lstset{style=custompy}
\begin{lstlisting}
import exonoodle
exonoodle.run('configuration.ini')
\end{lstlisting}

 An example of running script is also available in \texttt{exonoodle/scripts/run\_exonoodle.py}. 
 
\paragraph{} All functions in the software are documented with detailed doc-strings (help messages). Type \texttt{help(function)} in a python session with the imported package to access it. The user is not supposed to change any of the source file in the \texttt{exonoodle/} folder to shape the exoplanet system. The configuration file (\texttt{configuration.ini}), and the function \texttt{exonoodle.run('configuration.ini')} should be the only interface for a regular user. Should you have any problem with it, please report it to the author. [link to GIT bug report when public]. Although, \texttt{exonoodle} is an open-source project, you are free to contribute either with your own code or with suggestions. Do not hesitate to contact the author for more details.
\paragraph{} To ensure the installation was rightly done and that you have the correct configuration to run the software, you can launch a test session by typing in your terminal:
\lstset{style=custombash}
\begin{lstlisting}
py.test exonoodle/
\end{lstlisting}

\section{\textsc{Interfaces}}
\subsection{\textsc{Input}}
\paragraph{\textsc{The configuration file}} The software takes all its information from a configuration file, with which it creates a dictionary (see fig.\ref{SequentialDescription}). In its minimalist form, a model of the file is presented in Appendix, fig.\ref{ConfigurationFileMinimalist}. If the file doesn't contain, at least, this information, the code will not be able to run. By default, all bodies are considered as black-bodies and the albedo default value is set at 0. With this example, you would launch a single run. The output would tell you which are the default values it took (example in the Appendix, fig.\ref{OutRunMinimum}).
\vspace*{-0.4cm}

\subparagraph{} An extended file example is shown in fig.\ref{ConfigurationFileShape}, containing all the possible parameters taken by the software. The file is organized with five sections in a nested structure compatible with \texttt{ConfigObj} package. The available parameters are the following (all the \textcolor{gray}{\texttt{gray}} parameters are optional, the \textcolor{RoyalBlue}{\texttt{blue}} parameters are mandatory, the \textcolor{orange}{\texttt{orange}} parameter is a list of options):
\begin{outline}
\1 \texttt{[exonoodle]}: Here are the parameters used to choose between the different types of possible runs in the package.\\
      Note that what is mentionned here will define the type of run that will be performed between a single run, a sequential run or a parallel run. If this section is empty, it will perform a single run at mid-transit time. If a \texttt{phase} is mentionned, it will perform a single run at the given orbital phase ($\varphi$). If a \texttt{time\_sampling} is mentionned alone, it will perform a light-curve (several time instants) between $\varphi=0$ and $\varphi=1$. If, in addition to \texttt{time\_sampling}, a \texttt{np\_proc} is here, the light-curve run will be parallelized. A folder will be created in the directory from which you launch the run, unless you specify another \texttt{folder} path. Also note that $\varphi=0$ at mid-transit by convention.
    \2 \textcolor{gray}{\texttt{nb\_proc}}: How many CPUs will be used for the computation, no dynamical allocation. (0 and 1 will both result in a sequential run.) [default = 0]
    \2 \textcolor{gray}{\texttt{folder}}: Path to the folder you want to store your results in. [default = none]
    \2 \textcolor{gray}{\texttt{phase}}: Phase, for a single spectra generation.  (phase = 0 at mid-transit) [default = 0]
    \2 \textcolor{gray}{\texttt{time\_sampling}}: Sampling of the light-curve, this can represent the integration time for a specific instrument. [default = none ; see above for more details]
    \2 \textcolor{gray}{\texttt{phase\_min}}: Start time of the observation. Between $-1$ and $1$. [default = 0]
    \2 \textcolor{gray}{\texttt{phase\_max}}: End time of the observation. Between $-1$ and $1$. [default = 1]
    \2 \textcolor{gray}{\texttt{time\_sampling\_unit}}: Unit of the time sampling (has to be a time unit) [default = second]

\1 \texttt{[observation]}: Here are the parameters of the observation\\
	Make sure all your input files (if you have any) are consistent with this wavelength range. If not, they will be extrapolated, which may not be desirable.
	\2 \textcolor{RoyalBlue}{\texttt{wave\_min}}: Lower boundary of the wavelengh range for the spectrum
	\2 \textcolor{RoyalBlue}{\texttt{wave\_max}}: Upper boundary of the wavelengh range for the spectrum
	\2 \textcolor{RoyalBlue}{\texttt{dwave}}: Sampling of the spectrum.
	\2 \textcolor{gray}{\texttt{wave\_unit}}: Unit of the wavelength specified above (has to be a length unit) [default = micron]

\1 \texttt{[star]}: Here are the parameters to define the star for your observation.\\
	Be aware that the mandatory parameter will be ignored if you include a more complex input, e.g. if you specify a file to read the emission spectrum, the temperature will be ignored.
 	\2 \textcolor{RoyalBlue}{\texttt{name}}: Name of the star
 	\2 \textcolor{gray}{\texttt{spectral\_type}}: Spectral type of the star [default = none]
 	\2 \textcolor{gray}{\texttt{spectral\_file}}: Path to the file containing the stellar spectrum. [default = none]
 	\2 \textcolor{RoyalBlue}{\texttt{mass}}: Mass of the star
 	\2 \textcolor{RoyalBlue}{\texttt{radius}}: Radius of the star
 	\2 \textcolor{RoyalBlue}{\texttt{temperature}}: Temperature of the star
 	\2 \textcolor{RoyalBlue}{\texttt{distance}}: Distance of the system from the observer.
 	\2 \textcolor{gray}{\texttt{LD\_file}}: Path to the file containing the limb-darkening coefficients. [default = none]
 	\2 \textcolor{gray}{\texttt{LD\_coef}}: Limb-darkening coefficients (not $\lambda$-dependant), separated by a coma. [default = list(0)]
 	\2 \textcolor{orange}{\texttt{LD\_method}}: Limb-darkening method. Choice between: none, claret4, quadratic
 	\2 \textcolor{gray}{\texttt{mass\_unit}}: Unit of the stellar mass [default = solMass]
 	\2 \textcolor{gray}{\texttt{radius\_unit}}: Unit of the stellar radius [default = solRad]
 	\2 \textcolor{gray}{\texttt{temperature\_unit}}: Unit of the stellar temperature [default = K]
 	\2 \textcolor{gray}{\texttt{distance\_unit}}: Unit of the star distance from the observer [default = pc]

\1 \texttt{[planet]}: Here are the parameters to define the exoplanet\\
Like for the star, the most detailed information will be considered for the run. If a file is entered, it will pre-empt the corresponding parameter. (e.g. if you give a $\lambda$-dependant radius for the absorption, the radius you mention will be ignored.)
 	\2 \textcolor{RoyalBlue}{\texttt{name}}: Name of the planet
 	\2 \textcolor{RoyalBlue}{\texttt{mass}}: Mass of the planet
 	\2 \textcolor{RoyalBlue}{\texttt{radius}}: Radius of the planet
 	\2 \textcolor{gray}{\texttt{absorption\_file}}: Path to the file containing the $\lambda$-dependant planetary absorption (in $R_p / R_\bigstar$ [default = none]
 	\2 \textcolor{RoyalBlue}{\texttt{day\_temperature}}: Temperature of the day side of the planet
 	\2 \textcolor{gray}{\texttt{day\_emission\_file}}: Path to the file containing the planetary day side emission spectrum [default = none]
 	\2 \textcolor{RoyalBlue}{\texttt{night\_temperature}}: Temperature of the night side of the planet
 	\2 \textcolor{gray}{\texttt{night\_emission\_file}}: Path to the file containing the planetary night side emission spectrum [default = none]
 	\2 \textcolor{gray}{\texttt{phase\_shift}}: angle of shift of the hotspot of the planet. Will shift the planet day/night phasing. [default = 0]
 	\2 \textcolor{gray}{\texttt{albedo}}: Albedo of the planet. [default = 0]
 	\2 \textcolor{gray}{\texttt{albedo\_file}}: Path to the file containing the $\lambda$-dependant Albedo of the planet. [default = none]
 	\2 \textcolor{gray}{\texttt{mass\_unit}}: Unit of the planetary mass [default = jupiterMass]
 	\2 \textcolor{gray}{\texttt{radius\_unit}}: Unit of the planetary radius [default = jupiterRad]
 	\2 \textcolor{gray}{\texttt{temperature\_unit}}: Unit of the planetary temperatures [default = K]
 	\2 \textcolor{gray}{\texttt{phase\_shift\_unit}}: Unit of the planetary phase-shift [default = deg]

\1 \texttt{[orbit]}: Here are the parameters to define the orbit of the planet around the star
 	\2 \textcolor{RoyalBlue}{\texttt{orbital\_period}}: Orbital period of the planet
 	\2 \textcolor{RoyalBlue}{\texttt{inclination}}: Inclination of the orbit
 	\2 \texttt{semi\_major\_axis}: Semi-major axis of the orbit (considered circular)
 	\2 \textcolor{gray}{\texttt{orbital\_period\_unit}}: Unit of the orbital period [default = day]
 	\2 \textcolor{gray}{\texttt{inclination\_unit}}: Unit of the inclination [default = deg]
 	\2 \textcolor{gray}{\texttt{semi\_major\_axis\_unit}}: Unit of hte semi-major axis [default = AU]


\end{outline}




\newgeometry{top=1.3cm, bottom=1.5cm}
\thispagestyle{plain}
\restylefloat{figure}
\begin{figure}

\lstset{style=custompyconfig,
morecomment=[l][\color{Gray}]{\#},
morecomment=[s][\color{LimeGreen}]{[}{]},
}
\begin{lstlisting}
[exonoodle]
nb_proc = 4
folder = "output"
phase = 0.3
time_sampling = 10
phase_min = -0.3
phase_max = 0.7

time_sampling_unit = second

[observation]
wave_min = 3
wave_max = 11
dwave = 0.006
wave_unit = micron

[star]
name = WASP-43a
spectral_type = K7
spectral_file = source/Wasp43_star_microJy.fits
mass = 0.713
radius = 0.66
temperature = 4536.0
distance = 80.0
LD_file = source/WASP43_LDCs_A100.txt
LD_coeff = 0.2, 0.5
LD_method = quadratic

mass_unit = solMass
radius_unit = solRad
temperature_unit = K
distance_unit = pc

[planet]
name = WASP-43b
mass = 2.029
radius = 1.034
absorption_file = source/HotJupiter_Absorption_Clear.dat
day_temperature = 1712.0
day_emission_file = source/W43b_Fix_NoTiO_NoClouds_Long+000_model_3um0.dat
night_temperature = 830.0
night_emission_file = source/W43b_Fix_NoTiO_NoClouds_Long-180_model_3um0.dat
phase_shift = 0
albedo = 0.20
albedo_file = source/WASP43b_Albedo.dat

mass_unit = jupiterMass
radius_unit = jupiterRad
temperature_unit = K
phase_shift_unit = deg

[orbit]
orbital_period = 0.81347437
inclination = 82.64
semi_major_axis = 0.01524

orbital_period_unit = day
inclination_unit = deg
semi_major_axis_unit = AU

\end{lstlisting}
\caption{Exemple of the configuration file (in .txt) with maximum information, here for the WASP-43 system. This file is consistant with ConfigObj package.}
\label{ConfigurationFileShape}

\end{figure}
\restoregeometry

\paragraph{\textsc{Units}} The computation is done with absolute units for flux, distances, times, etc. It is important that the units entered in the configuration file and the pointed source files are correct as they will be read by \texttt{exonoodle}. All the units shall be consistent with \texttt{astropy.units}\footnote{\texttt{astropy.units} documentation : \href{http://docs.astropy.org/en/stable/units/}{\ding{254} docs.astropy.org}}, as all the numerical values are taken care of as \texttt{quantities}. The \texttt{astropy.units} are mostly consistent with IAU style guidelines\footnote{IAU style guidelines : \href{https://www.iau.org/static/publications/stylemanual1989.pdf}{\ding{254} www.iau.org}}.
\paragraph{\textsc{Files}} The configuration file can contain paths to data files used as input, to be read by the program. You are invited to use \emph{.fits} format with a full and correct header (especially units), or \emph{.dat} or \emph{.txt}. It is recomended to use the  \emph{ESCV 0.9}\footnote{\texttt{astropy.io.ascii.ecsv} documentation: \href{https://docs.astropy.org/en/stable/api/astropy.io.ascii.Ecsv.html}{\ding{254} docs.astropy.org}} convention. In any case, make sure your file is compatible with the Astropy \texttt{ascii.read()} routine if you have a text file.
\paragraph{\textsc{Contradictions}} If contradictory information are given in the file (for example a temperature not compatible with the values in the spectrum file of a body), the most detailed information will be used (here, the file).


\subsection{\textsc{Output}}
\paragraph{} The program creates automatically a new \texttt{Noodle/} folder with the date so as not to erase your previous modelling. If a folder with that day already exists, it creates a new one with a suffix. In this folder, you will find all the files generated by \texttt{exonoodle} (between [], the files that are not always created, depending on the kind of run and its options):
\begin{itemize}
\item[•] \texttt{SED\_something.dat} - Multiple data files for the system spectrum (star+planet). Only one file in case of a single run (\texttt{SED\_0.dat}), or a group of numbered files in case of the simulation of a lightcurve.
\item[•] \texttt{configuration.ini} - A copy of the configuration file used by the software to create the files.
\item[•] [\texttt{times.dat}] - If you have created a lightcurve, here is the file containing the table to link a given phase or time to its corresponding index for the series of data files.
\end{itemize}
\begin{figure}[t]
\lstset{style=custompy, frame=single}
\begin{lstlisting}
# %ECSV 0.9
# ---
# datatype:
# - {name: wavelength, unit: micron, datatype: float64}
# - {name: flux, unit: microJansky, datatype: float64}
# schema: astropy-2.0
#
#
#  - - File generated by ExoNoodle - -
#         V0.10.0 - MML (CEA-Saclay)
#       2020-01-14 14:11:28.788730
wavelength flux
3.00000E+00        8.51647E+04
3.00600E+00        8.49302E+04
3.01200E+00        8.46967E+04
3.01800E+00        8.44640E+04
3.02400E+00        8.42321E+04
...	               ...
\end{lstlisting}
\caption{Example of the SED output file (in .txt) with the complete header and the two columns. (Note : here the table is cropped after a few lines)}
\label{outputFile}
\end{figure}

\floatstyle{plain}
\restylefloat{figure}
\paragraph{} All the output data files of the software are constructed with the same convention. It is a \textit{.dat} file with a header and two columns (see fig.\ref{outputFile}). This file is compatible with \textit{MIRISim}, to be called as an external SED source file. Reading this file in Python is possible with :
\lstset{style=custompy}
\begin{lstlisting}
from astropy.io import ascii

data = ascii.read("SED_something.dat")                          # Load data from file
flux = data["flux"].data * data["flux"].unit                    # Flux as quantity with unit
wavelength = data["wavelength"].data * data["wavelength"].unit  # Wavelength as quantity with unit
\end{lstlisting}
\paragraph{} An example of the script is available in \texttt{exonoodle/scripts/exonoodle\_lightcurve\_plot.py} to plot a light-curve at a given wavelength. It contains one example of a way to extract a value for each file and plot it. It is meant to help you read and exploit the simulation results.
\begin{note}
Any file formatted as the one presented in fig.\ref{outputFile} is easily supported. Therefore, the \emph{ESCV 0.9} formatting is recommended for your input files. You can find files examples in the \texttt{Data/} folder of the repository.
\end{note}

\section{\textsc{Sequential description}}
\begin{figure}[ht]
\includegraphics[width=15.92cm]{figures/SequentialDescription.pdf}
\caption{Description of the interaction within the major functions and variables of the user (external), of the software (internal) and of the computation itself (calculation)}
\label{SequentialDescription}
\end{figure}
\paragraph{}Fig.\ref{SequentialDescription} describes the sequence of a computation, for a single run. Be aware that the nomenclature used in Fig.\ref{SequentialDescription} is not the same as that used in the software itself. Three different types of variable or objects are described:
\vspace*{-0.4cm}
\subparagraph{\textsc{External}} First, the user defined inputs are highlighted in grey. The variable are represented in light grey and in dark grey the files themselves.
\vspace*{-0.4cm}
\subparagraph{\textsc{Calculation}} Then, in blue are the elements necessary for the computation, that will be created by the software based on the information given in the configuration by the user. In light blue are the variables or dictionary. The darker boxes are the objects and their content (method, specified). Note that any file you provide with a $\lambda$-dependant variable will be interpolated on this array which you specified in the configuration file.





\section{\textsc{Further development}}
\paragraph{} Here we presented the current version of \texttt{exonoodle}. The following improvement for the next version are already foreseen:
\begin{itemize}
\item[•] Add more limb-darkening laws
\item[•] Add a differential fading of the planet during eclipse ($\Lambda_{day} \neq \Lambda_{night}$)
\end{itemize}
\paragraph{} \texttt{exonoodle} is an open source project under GNU/GPLv3 license. Any contribution is welcome, either developpements, bug reports or additional feature wishes. Please feel free to reach out to the authors.
% % % % % % % % % % % % % % % % % % % BIBLIOGRAPHIE % % % % % % % % % % % % % % % % % % % %
%\section*{\center{\textsc{Bibliographie}}}\addcontentsline{toc}{section}{\textsc{Bibliographie}}
%\bibliographystyle{plain}
%\bibliography{RapportStage_LMD_MartinLagarde_Biblio.bib}




\newpage


\section*{\textsc{\begin{center}
APPENDIX
\end{center}}}


\restylefloat{figure}
\begin{figure}[h!]
\lstset{style=custompyconfig,
morecomment=[l][\color{Gray}]{\#},
morecomment=[s][\color{LimeGreen}]{[}{]},
}
\begin{lstlisting}
[exonoodle]


[observation]
wave_min = 3
wave_max = 11
dwave = 0.006


[star]
name = WASP-43a
spectral_type = K7
mass = 0.713
radius = 0.66
temperature = 4536.0
distance = 80.0
LD_method = none


[planet]
name = WASP-43b
mass = 2.029
radius = 1.034
day_temperature = 1712.0
night_temperature = 830.0


[orbit]
orbital_period = 0.81347437
inclination = 82.64
semi_major_axis = 0.01524
\end{lstlisting}
\caption{Exemple of the configuration file (in .txt) with the minimum information, here for the WASP-43 system.}
\label{ConfigurationFileMinimalist}
\end{figure}



\begin{figure}[h!]
\includegraphics[width=17cm]{figures/lightcurve_5um.pdf}
\caption{Diagnosis figure obtained by running the \texttt{exonoodle\_lightcureve\_plot.py} routine.}
\label{plotTransitFigure}
\end{figure}


% \floatstyle{boxed}
% \restylefloat{figure}
\begin{figure}[h!]
\begin{verbatim}
------------------------------------------------------------------------------------
                                    -   ExoNoodle   -
                                INSTANTANEOUS SPECTRUM RUN
------------------------------------------------------------------------------------
14:41:50 INFO      Version 0.12.2 - CEA-Saclay

14:41:50 WARNING  star : limb darkening : deactivated - Flat star disc considered.
14:41:50 WARNING  star : spectrum_file: no file defined ;
                                                Default blackbody with T = 4536.0 K
14:41:50 WARNING  planet : day_emission_file: no file defined ;
                                                Default blackbody with T = 1712.0 K
14:41:50 WARNING  planet : night_emission_file: no file defined ;
                                                Default blackbody with T = 830.0 K
14:41:50 WARNING  planet : reflection : no file or value defined ;
                                                albedo defined to 0.538 (Jupiter geometric albedo)
14:41:50 WARNING  planet : transmission : no file defined ; no atmosphere considered
14:41:50 INFO     >> Directory created: Noodles_2020-04-11_13/
14:41:50 DEBUG    >> Star Creation
14:41:50 DEBUG            > Star WASP-43a
14:41:50 DEBUG    >> Planet Creation
14:41:50 DEBUG            > Planet WASP-43b as Planet1
14:41:50 DEBUG             • Day Emission
14:41:50 DEBUG             • Night Emission
14:41:50 DEBUG             • Reflection
14:41:50 DEBUG             • Transmission
14:41:50 DEBUG    >> System Contribution Creation (with star WASP-43a and planet WASP-43b)
14:41:50 DEBUG            > System WASP-43
14:41:50 DEBUG             • You are now in transit (After mid-transit time)
14:41:50 DEBUG             ... STAR WEAKED IN TRANSITION + PLANET TRANSMISSION + PLANET EMISSION
14:41:50 DEBUG    >> SED file creation
14:41:50 DEBUG         . Writing file:Noodles_2020-04-11_13/SED_0.dat

14:41:50 INFO          ... Done with success, files Noodles_2020-04-11_13/SED_index.dat
14:41:50 INFO
exoNoodle run: 0.12 s
                                                          *** exoNoodle ***   End
\end{verbatim}
\caption{Consol output for a single run example with some approximation warning}
\label{OutRunMinimum}
\end{figure}


\begin{figure}[h!]
\begin{verbatim}
------------------------------------------------------------------------------------
                                    -   ExoNoodle   -
                                INSTANTANEOUS SPECTRUM RUN
------------------------------------------------------------------------------------
12:16:52 INFO      Version 0.12.2 - CEA-Saclay

12:16:52 INFO     >> Directory created: Noodles_2020-04-11/
12:16:52 DEBUG    >> Star Creation
12:16:52 DEBUG            > Star WASP-43a
12:16:52 DEBUG               . Reading file: DATA/Wasp43_star_microJy.fits
12:16:52 DEBUG               . extensionFile: .fits
12:16:52 DEBUG               . Convert: wavelength unit from micron to micron
12:16:52 DEBUG               . Convert: data unit from uJy to mJy
12:16:52 DEBUG    >> Planet Creation
12:16:52 DEBUG            > Planet WASP-43b as Planet1
12:16:52 DEBUG             • Day Emission
12:16:52 DEBUG               . Reading file: Data/W43b_Fix_NoTiO_NoClouds_Long+000_model_3um0.dat
12:16:52 DEBUG               . extensionFile: .dat
12:16:52 DEBUG               . Convert: wavelength unit from micron to micron
12:16:52 DEBUG               . Convert: data unit from  to
12:16:52 DEBUG               . Flux type: flux_ratio
12:16:52 DEBUG             • Night Emission
12:16:52 DEBUG               . Reading file: Data/W43b_Fix_NoTiO_NoClouds_Long-180_model_3um0.dat
12:16:52 DEBUG               . extensionFile: .dat
12:16:52 DEBUG               . Convert: wavelength unit from micron to micron
12:16:52 DEBUG               . Convert: data unit from  to
12:16:52 DEBUG               . Flux type: flux_ratio
12:16:52 DEBUG             • Reflection
12:16:52 DEBUG               . extensionFile: .dat
12:16:52 DEBUG               . Reading file: Data/WASP43b_Albedo.dat
12:16:52 DEBUG             • Transmission
12:16:52 DEBUG               . extensionFile: .dat
12:16:52 DEBUG               . Reading file: Data/WASP43b_Absorption.dat
12:16:52 DEBUG    >> System Contribution Creation (with star WASP-43a and planet WASP-43b)
12:16:52 DEBUG            > System WASP-43
12:16:52 DEBUG             • You are now in transit (After mid-transit time)
12:16:52 DEBUG             ... STAR WEAKED (LIMB DARKENING) + PLANET TRANSMISSION + PLANET EMISSION
12:16:53 DEBUG    >> SED file creation
12:16:53 DEBUG         . Writing file:Noodles_2020-04-11/SED_0.dat

12:16:53 INFO          ... Done with success, files Noodles_2020-04-11/SED_index.dat
12:16:53 INFO
exoNoodle run: 1.1 s
                                                          *** exoNoodle ***   End
\end{verbatim}
\caption{Consol output for a single run with full options in configuration file completed.}
\label{OutRunMinimum}
\end{figure}


\end{document}
