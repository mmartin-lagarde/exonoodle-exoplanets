#!/usr/bin/env python
# -*- coding: utf-8 -*-

from astropy import units as u

stellar_stability = 15 *u.min # Granulation supposed stable during 30mn, conservative assumption with divided by 2 (Cf. Camilla Danielsky)

