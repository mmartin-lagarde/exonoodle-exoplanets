 ## Contributing to exonoodle

 ### Reporting issues

 If you find a bug or other unexpected behavior while using `exonoodle`, open an issue
 on the [GitLab repository](https://gitlab.com/mmartin-lagarde/exonoodle-exoplanets/-/issues). If you have
 a feature request or general question about functionality, the issues page is the best
 place to seek answers. 

 If you report an issue, please give the necessary details (python environment, `exonoodle version)
 and code required to reproduce the problem.


 ### Contributing code

 We gladly welcome all contributions to the code, so please don't be shy about opening
 a pull request. If you want to make a substantial change to the code, please open an 
 issue first to describe your plan so we can discuss the best course of action.

 ## Setting up your development environment

 If you want to make changes to the code, you should fork the repository and clone it 
 to your local machine

 ```
 git clone https://gitlab.com/YOURUSERNAME/exonoodle.git
 cd exonoodle
 git checkout -b BRANCHNAME
 ```

 where `BRANCHNAME` describes your contribution.