#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                            TRANSIT GENERATOR                             #
#                      TEST FUNCION - SEDfileCreation                      #
# --------------------------------------------------------------------------#
#
# CONTRUBUTORS :
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#
# Part of TRANSIT GENERATOR program / Test for ToolBox
# Shall be in ./tests


# External library imports
import numpy.testing as npt
import numpy as np
import os
from astropy import units as u

# Built-in library imports
from exonoodle.utils import SEDfileCreation, WavelengthDepdtVariable

TEST_DIR = os.path.dirname(__file__)


def test_SEDfileCreation():
    """
    Test SED file Creation

    :return:
    :rtype:
    """
    status = True
    full_filename = os.path.join(TEST_DIR, 'tests.txt')

    waves1 = (np.arange(4) + 10.) * u.micron
    fluxes1 = (np.arange(4) * 6 + 2.) * u.mJy

    SEDfileCreation(waves1, fluxes1, full_filename)

    SED_verification = WavelengthDepdtVariable(full_filename)

    npt.assert_equal(waves1, SED_verification.wavelength)

    npt.assert_equal(fluxes1, SED_verification.data)

    os.remove(full_filename)
