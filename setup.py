#!/usr/bin/env python

"""
Setup file for installing KISS
"""

import io
import os
import re

from setuptools import setup, find_packages


def read(*names, **kwargs):
    with io.open(
        os.path.join(os.path.dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf8")
    ) as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name="exoNoodle",
    version=find_version("exonoodle", "version.py"),
    description="exoNoodle",
    url="https://gitlab.com/mmartin-lagarde/exonoodle-exoplanets/",
    author="Marine MARTIN-LARGADE",
    author_email="marine.martin-lagarde@cea.fr",
    packages=find_packages(),
    package_dir={'exonoodle.tests': 'exonoodle/tests'},
    package_data={'exonoodle': ["configspec.ini"]},
    scripts=['exonoodle/cmdline/exonoodle'],
    data_files=[('', ['README.adoc'])]
)
