#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Run a exoNoodle simulation for a given exonoodle spectrum
"""
import exonoodle

#Initialisation of the variables
filename = "configuration.ini" # The filename with all the characteristics of your system

exonoodle.run(filename)
