#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
usefull source: https://stackoverflow.com/questions/5442910/python-multiprocessing-pool-map-for-multiple-arguments
"""

from astropy import units as u
import numpy as np
import os
import time as t
import shutil
from . import compute
from . import utils
from .version import __version__
from . import config

import logging
LOG = logging.getLogger('exonoodle.single_spectrum')

def single_spectrum(params):
    """

    :param params:
    :type params:
    :param time:
    :type time: float
    :return:
    :rtype:
    """

    utils.init_log(log="exonoodle.log", stdout_loglevel="DEBUG", file_loglevel="INFO")

    print("\n--------------------------------------------------------------------------------------------------------------------")
    print("                                                     -   ExoNoodle   -         ")
    print("                                                  INSTANTANEOUS SPECTRUM RUN        ")
    print("--------------------------------------------------------------------------------------------------------------------")
    LOG.info(" Version {} - CEA-Saclay\n".format(__version__))
    start_time = t.time()

    if isinstance(params, str):
        configuration = config.read(params)
    else:
        configuration = params

    folder = configuration["exonoodle"]["folder"]
    phase = configuration["exonoodle"]["phase"]
    time = (2 * np.pi) * phase

    output_dir = utils.create_directory(folder)
    LOG.info('>> Directory created: {}/'.format(output_dir))

    padding = 1


    wavelength = config.init_waves(configuration)

    compute.phase_system_spectrum([time*u.rad], configuration, output_dir, 0, padding, wavelength)
    LOG.info('     ... Done with success, files {}/SED_index.dat'.format(output_dir))

    end_time = t.time()

    output_filename = os.path.join(output_dir, os.path.basename(configuration.filename))
    shutil.copy(configuration.filename, output_filename)

    LOG.info("\nexoNoodle run: {:.2g} s".format(end_time - start_time))
    print("                                                                                    *** exoNoodle ***   End")
