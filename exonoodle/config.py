from astropy import units as u
import numpy as np
import pkg_resources
import configobj
import validate
import os
import sys

import logging
LOG = logging.getLogger(__name__)


def get_nested(data, args):
    """
    Allow to get tmp in dictionnary tree

    Used in ConfigObj validator

    If ones want to get toto["section1"]["s2]["key"]
    call:
    tmp = get_nested(toto, ["section1", "s2", "key"])

    Parameter:
    :param dict data: input dict to get data on
    :param list(str) args: list of keys to use recursively

    :return: tmp corresponding to the list of keys
    """
    value = data.copy()
    for key in args:
        value = value.get(key)

    return value


def read(filename, skip_warnings=False):
    """
    Read then validate and convert the input file
    BEWARE there must be a file named 'configspec.ini' in the directory of kiss

    :param str filename: .ini filename
    :param bool skip_warnings: To skip testing of parameters (used in main.run because the log is not initialized yet

    :return ConfigObj:  config file
    """

    if not os.path.isfile(filename):
        LOG.error("The file '{}' can't be found".format(filename))
        sys.exit()

    # Prepare to convert values in the config file
    val = validate.Validator()
    specfile = pkg_resources.resource_filename('exonoodle', 'configspec.ini')
    configspec = configobj.ConfigObj(specfile, list_values=False)

    config = configobj.ConfigObj(filename, configspec=configspec, raise_errors=True)

    # Check and convert values in config.ini (i.e str to float or integer/bool)
    results = config.validate(val, preserve_errors=True)

    for entry in configobj.flatten_errors(config, results):

        [section_list, key, error] = entry
        section_list.append(key)

        if not error:
            msg = "The parameter %s was not in the config file\n" % key
            msg += "Please check to make sure this parameter is present and there are no mis-spellings."
            raise ValueError(msg)

        if key is not None and isinstance(error, validate.VdtValueError):
            option_string = get_nested(configspec, section_list)
            msg = "The parameter {} was set to {} which is not one of the allowed values\n".format(
                key, get_nested(config, section_list))
            msg += "Please set the tmp to be in {}".format(option_string)
            raise ValueError(msg)

    # Create quantities
    parameter_list = [
         ("exonoodle", "time_sampling")
        , ("star", "mass")
        , ("star", "radius")
        , ("star", "temperature")
        , ("star", "distance")
        , ("planet", "mass")
        , ("planet", "radius")
        , ("planet", "day_temperature")
        , ("planet", "night_temperature")
        , ("planet", "phase_shift")
        , ("orbit", "orbital_period")
        , ("orbit", "inclination")
        , ("orbit", "semi_major_axis")
    ]

    # Get value and unit, replace value with the value with quantity, then delete the unit parameter from the dict
    for p in parameter_list:
        value = get_nested(config, p)

        # Get location of unit based on location of the value
        pu = list(p)
        pu[-1] = pu[-1] + "_unit"
        unit = get_nested(config, pu)

        quantity_value = (value * u.Unit(unit)).decompose()

        # Update value with quantity value
        config[p[0]][p[1]] = quantity_value

        # Delete unit from dictionnary
        del(config[pu[0]][pu[1]])

    phase_min = config["exonoodle"]["phase_min"]
    phase_max = config["exonoodle"]["phase_max"]

    if phase_max <= phase_min:
        raise ValueError(f"phase_max ({phase_max}) must be bigger than phase_min ({phase_min}).")

    if not skip_warnings:
        # Warnings and default values

        star_config = config["star"]

        if star_config["LD_method"] == 'none':
            LOG.warning("star : limb darkening : deactivated - Flat star disc considered.")
        elif star_config["LD_file"] == 'none':
            coeff = star_config["LD_coeff"]
            LOG.warning(f"star : limb darkening : no file, using white coefficients: {coeff}")


        tmp_param = [(("star", "spectral_file"),         ("star", "temperature")),
                     (("planet", "day_emission_file"),   ("planet", "day_temperature")),
                     (("planet", "night_emission_file"), ("planet", "night_temperature"))]

        for (p, dp) in tmp_param:
            value = get_nested(config, p)
            if value == "none":
                default_temperature = get_nested(config, dp)
                LOG.warning(f"{p[0]} : {p[1]} : no file defined ; Default blackbody with T = {default_temperature}")

        if config["planet"]["albedo_file"] == "none":
            if config["planet"]["albedo"] == 0:
                LOG.warning(f"planet : reflection : no file or value defined ; white albedo defined to {config['planet']['albedo']}")
            else:
                LOG.warning(f"planet : reflection : no file ; white albedo defined as {config['planet']['albedo']}")

        if config["planet"]["absorption_file"] == "none":
            LOG.warning("planet : transmission : no file defined ; no atmosphere considered")

    return config


def init_waves(configuration):
    """
    Create wavelength sampling from the configuration file

    :param ConfigObj configuration: input configuration file

    :return: wavelength sampling
    :rtype: np.array(float) (quantity)
    """

    wave_min = configuration["observation"]["wave_min"]
    wave_max = configuration["observation"]["wave_max"]
    dwave = configuration["observation"]["dwave"]
    wave_unit = configuration["observation"]["wave_unit"]

    wavelength = np.arange(wave_min, wave_max+dwave, dwave) * u.Unit(wave_unit)

    return wavelength