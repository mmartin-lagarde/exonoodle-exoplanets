#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                    exoNoodle : Instantaneous spectrum                     #
#                               -- UTILS --                                 #
# --------------------------------------------------------------------------#
#
#
# CONTRIBUTORS :
# --------------
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#
# Part of exoNoodle package
# Main : system_instant_spectrum() in compute.py
# (TBI : To be integrated)
# All units are astropy.units
#
# Developped in Python 3
# Suitable for Python 2.7
#
#
#  CONTAINS :
# -----------
#   functions:
#       blackBody
#       readConfiguration, writeConfiguration, getInConf
#       compareSource
#       selectSpectrum
#       geometryTransition
#       SEDfileCreation
#       getVersion
#       progressBar
#   class WavelengthDepdtVariable
#        read, __read_ascii, __read_fits, writeFits, writeAscii, convert_to, compare, plot
#        init, getInterpolated


# External library imports
import os
import numpy as np
import matplotlib.pyplot as plt
from astropy.io import ascii, fits
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz as cumulative_integration
from astropy import units as u
import math
import datetime
import sys
import glob
from astropy.modeling import models
from astropy.table import Table, Column

import logging

from .version import __version__

LOG = logging.getLogger(__name__)


# >>>>>>>>>>> STATUS VARIABLES.
now = datetime.datetime.now()
# >>>>>>>>>>> GLOBAL VARIABLES.
pi = math.pi


def init_log(log="exonoodle.log", stdout_loglevel="DEBUG", file_loglevel="DEBUG"):
    """

    :param str log: filename where to store logs. By default "exonoodle.log"
    :param str stdout_loglevel: log level for standard output (ERROR, WARNING, INFO, DEBUG)
    :param str file_loglevel: log level for log file (ERROR, WARNING, INFO, DEBUG)
    :return:
    :rtype:
    """

    import logging.config

    log_config = {
        "version": 1,
        "formatters":
            {
                "form01":
                    {
                        "format": "%(asctime)s %(levelname)-8s %(message)s",
                        "datefmt": "%H:%M:%S"
                    },
                "form02":
                    {
                        "format": "%(asctime)s [%(name)s] %(levelname)s - %(message)s",
                        "datefmt": "%H:%M:%S"
                    },
            },
        "handlers":
            {
                "console":
                    {
                        "class": "logging.StreamHandler",
                        "formatter": "form01",
                        "level": stdout_loglevel,
                        "stream": "ext://sys.stdout",
                    },
                "file":
                    {
                        "class": "logging.FileHandler",
                        "formatter": "form02",
                        "level": file_loglevel,
                        "filename": log,
                        "mode": "w",  # Overwrite file if it exists
                    },
            },
        "loggers":
            {
                "":
                    {
                        "level": "NOTSET",
                        "handlers": ["console", "file"],
                    },
            },
        "disable_existing_loggers": False,
    }

    logging.config.dictConfig(log_config)


def blackBody(wavelength, temperature, radius, distance):
    """
        . Function to compute a black body spectrum for a wavelength range (wl) and
        a temperature as recieved in Earth orbit (apparent flux density derivated
        from frequency).

        . Developped for transitGenerator project
        (Creation 2017/10/17 (MML) -- Last update 2018/02/13 (RG))

        . from astropy/analytic_functions/tests/test_blackbody, function test_blackbody_synphot

        >>>> PARAMETERS
        • wavelength          : 1D quantity array - wavelength
        • temperature : quantity - body effective temperature
        • radius      : quantity - radius of the emitting body (sphere)
        • distance    : quantity - distance of the body from Earth


        >>>> RETURNS
        • flux    : 1D quantity array - same size as wl
        blackbody spectrum in [J m / micron3]  (SI)

        >>>> Example
        --------
        >>>   blackBody(10.*u.micron, 5980*u.K, 1.*u.solRad, 10*u.pc).to(u.Jy)
        2.332 instead of 2.382 Jansky   sun at 10 parsec
        """
    angle = radius.decompose() / distance.decompose()
    # solid angle  in steradian
    solid_angle = np.pi * angle * angle * u.sr

    # Since Astropy 4.0
    bb = models.BlackBody(temperature)
    flux = bb(wavelength) * solid_angle

    flux_mJy = flux.to(u.milliJansky, u.spectral_density(wavelength))

    return flux_mJy


def time2phase(time, period, eccentricity=0, true_anomaly=None):
    """
    Convert a time duration into radians assuming an orbital period to do the conversion

    :param float time: time (by default in seconds) as a quantity
    :param quantity period: orbital period (quantity of time)
    :param eccentricity:
    :param true_anomaly:

    :return: phase in radians
    """

    phase = time * (2 * np.pi * u.rad) / period
    phase.to(u.rad)

    return phase


def str2float(s):
    """
        . Function to convert a string to float if the string is a number.

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update *NoUpdate*)

        >>>> PARAMETERS
        • s  : string

        >>>> RETURNS
        • s  : string or float
    """
    try:
        return float(s)
    except ValueError:
        return s


def create_directory(outside_directory=None):
    """
    Create the output directory

    The generic name is: Noodles_YYYY-MM-DD
    If the directory exists, a number is added at the end:
    Noodles_YYYY-MM-DD_number

    :return str: Directory name
    """
    today = datetime.date.today()
    if outside_directory in ['none', None]:
        dirName = 'Noodles_{}'.format(today)
    else:
        dirName = os.path.join(outside_directory, 'Noodles_{}'.format(today))

    if os.path.isdir(dirName):
        number = len(glob.glob("{}*{}".format(dirName, os.sep)))
        dirName = '{}_{}'.format(dirName, number)

    os.makedirs(dirName)  # Can create one folder, including its parent directory, i.e makedirs.

    return dirName


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END

def writeConfiguration(source, fileName, index=''):
    """
        . Function to write a configuration file from a dictionary. Mainly used for test and developpement
        purposes.

        . Developped for transitGenerator project - created to replace getInFile
        (Creation as getInFile 2017/10/27 (MML) -- Last update 2018/02/12 (RG))


        >>>> PARAMETERS
        • source    : dictionary (empty index)  or source object  ==> need index
        • fileName  : string - complete name of the configuration file
        • [index]   : string - initiate the index [optional][default='']
        • [verbose] : Boolean - To enable prints [optional][default=False]

        >>>> RETURNS
        /no returns/
    """
    if isinstance(source, dict):
        my_dict = source
    else:
        my_dict = source.__dict__
    ff = open(fileName, 'w')
    for key in my_dict.keys():
        variable = my_dict[key]
        # does not work var_name = index+key.capitalize()
        var_name = index + key[0].upper() + key[1:]
        if isinstance(variable, str):
            ff.write(var_name + "\t" + variable + "\t /noUnit/ \n")
            LOG.debug("write " + var_name)
        elif isinstance(variable, type(None)):
            pass  # if LimbDarkening=None
        else:
            if variable.size == 1:
                ff.write(var_name + "\t" + str(variable.value) + "\t" + variable.unit.to_string() + "\n")
                LOG.debug("write {}".format(var_name))
            else:
                LOG.warning("------------------> Warning :: {} dimension > 1, not written".format(key))
    ff.close()
    return


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END


def compareSource(source1, source2, relatvieTolerence=1e-7):
    """
        . Function to compare two sources or dictionaries. Mainly used for test and developpement
        purposes.

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update *NoUpdate*)


        >>>> PARAMETERS
        • source1             : dictionnary
        • source2             : dictionnary
        • [relatvieTolerence] : Float - Relative tolerence for comparisons [optional][default=1e-7]
        • [verbose]           : Boolean - To enable prints [optional][default=False]


        >>>> RETURNS
        • status        : Boolean - describe the state of the comparison.
    """
    status = True
    if isinstance(source1, dict):
        my_dict1 = source1
    else:
        my_dict1 = source1.__dict__
    #
    if isinstance(source2, dict):
        my_dict2 = source2
    else:
        my_dict2 = source2.__dict__

    for key in my_dict1.keys():
        variable1 = my_dict1[key]
        if key in my_dict2.keys():
            variable2 = my_dict2[key]
            if isinstance(variable1, str):
                status = status and (variable1 == variable2)
                LOG.info(key + " status =" + str(status))
            elif isinstance(variable1, type(None)):
                if variable2 is not None:
                    status = False
            else:
                np.allclose((variable1.decompose()).value, (variable2.decompose()).value, rtol=relatvieTolerence)
                LOG.info(key + " status =" + str(status))
        else:
            LOG.info(key + " is missing")
            status = False
    #
    return status


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def readSpectrum(wavelength, fileName='noValue', temperature='noValue', radius='noValue', distance='noValue',
                 verbose=False):
    """
        . Function to read an output spectrum for one of the source. Able to deal only
        with two different file extensions : FITS, DAT and TXT.
        . If no file is entered, the temperature is used to create a blackBody spectrum using blackBody()

        . Developped for transitGenerator project - created to replace readSpectra
        (Creation as readSpectra 2017/12/01 (MML) -- Last update 2018/01/24 (RG))

        >>>> PARAMETERS
        • wavelength    : 1D quantity array - wavelength
        • [fileName]    : String - Name of the entry file to read [optional][default='noValue']
        • [temperature] : Quantity - Body temperature - used if no fileName [optional][default='noValue']
        • [radius]      : Quantity - Body radius - used if no fileName [optional][default='noValue']
        • [distance]    : Quantity - Body distance to earth - used if no fileName [optional][default='noValue']
        • [verbose]     : Boolean - To enable prints [optional][default=False]

        >>>> RETURNS
        • myFlux        : 1D quantity array - same size as wl
        • wavelength    : 1D quantity array - wavelength from file or from wl
        • data_name     : String - Name of the column in the file to flag 'flux_ratio' =/= absolute flux
        """
    data_name = 0
    # Manage two different file extensions : fits and txt

    if fileName != 'none':
        LOG.debug('           . Reading file: {}'.format(fileName))
        mySpectrum = WavelengthDepdtVariable(fileName=fileName)
        if mySpectrum.data_name != 'flux_ratio':
            mySpectrum.convert_to(data_unit=u.mJy, wave_unit=u.micron, is_flux=True)
        else:
            data_name = mySpectrum.data_name
            mySpectrum.convert_to(data_unit=u.dimensionless_unscaled, wave_unit=u.micron, is_flux=True)

        if wavelength is not None:
            ## RG version 1.0.1
            ## beware getInterpolated extrapolate, but conservative_interpolation does not extrapolate
            ##myFlux = mySpectrum.getInterpolated(wavelength)
            myFlux = conservative_interpolation(mySpectrum.wavelength.value, mySpectrum.data.value, wavelength.value)
            myFlux = myFlux*mySpectrum.data.unit
        else:
            myFlux = mySpectrum.data
            wavelength = mySpectrum.wavelength

        LOG.debug("           . Flux type: {}".format(data_name))

    elif (temperature != 'noValue') and (radius != 'noValue') and (distance != 'noValue'):
        myFlux = blackBody(wavelength, temperature, radius, distance)
    else:
        LOG.error("------------------> Failure :: No source file for the spectrum.")
        LOG.error("                               Not enough information to call blackBody() either.")
        sys.exit()

    return myFlux, wavelength, data_name


class WavelengthDepdtVariable(object):
    """
    Class to define a variable which depends on the wavelength, without regards to its dimension.

    . Developped for transitGenerator project
    (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

    """

    # http://docs.astropy.org/en/stable/units/quantity.html
    # decorator to check the type of the argument
    @u.quantity_input(wavelength='length')
    def __init__(self, fileName=None, wavelength=None, data=None, fill_value='extrapolate'):
        """
        . Initialises a WavelengthDepdtVariable class object.

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • [fileName]    : String - Name of the entry file to read [optional][default=None]
        • [wavelength]  : Quantity - wavelength array - monitored by decorator [optional][default=None]
        • [data]        : Quantity - data array if the variable is a spectrum [optional][default=None]
        • [fill_value]  : String - setting data to interpolate [optional][default='extrapolate']
        • [verbose]     : Boolean - To enable prints [optional][default=False]

        >>>> RETURNS
        /no returns/
        """

        self.fill_value = fill_value

        self.parameters = None

        if fileName is not None:
            self.__read(fileName)

        else:
            if (data is None) or (wavelength is None):
                LOG.error("------------------> Failure :: Please check entry parameters")
                raise ValueError("Please set fileName or data")
            else:
                self.wavelength = wavelength
                self.data = data
                self.fileName = None

        self.interpolator = interp1d(self.wavelength.value, self.data.value, fill_value=self.fill_value)

    def __read(self, fileName):
        """
        . Function to read the variable if it comes from a file. It will choose between the differents reading
        methods __read_ascii() or __read_fits()

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • fileName      : String - Name of the entry file to read
        • [verbose]     : Boolean - To enable prints [optional][default=False]

        >>>> RETURNS
        /no returns/
        """
        self.fileName = fileName
        unc, extensionFile = os.path.splitext(fileName)
        # print("read "+extensionFile)
        # extensionFile=fileName.split('.')[-1]
        LOG.debug("           . extensionFile: " + extensionFile)
        if extensionFile in [".txt", ".dat"]:
            self.data_name = self.__read_ascii(fileName)
        elif extensionFile == ".fits":
            self.data_name = self.__read_fits(fileName)
        else:
            LOG.error('------------------> Failure :: Entry file has to be .txt, .dat or .fits')

        return

    def __read_ascii(self, fileName):
        """
        . Function to read the variable if it comes from a ASCII file. Only takes |.dat or |.txt

        The format must be:
        - Only 2 columns. One named wavelength, and another for data, name unspecified.
        - Takes only files formatted in ECSV0.9 (from astropy-2.0)

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • fileName      : String - Name of the entry file to read

        >>>> RETURNS
        • data_name     : String - Name of the column in the file
        """
        data = ascii.read(fileName)

        self.parameters = [key.lower() for key in data.keys()]

        if len(data.keys()) != 2:
            raise ValueError("ASCII files valid only if 2 columns, with one named 'wavelength'.")

        wave_name = 'wavelength'
        data_name = ''
        for item in data.keys():
            if item != wave_name:
                data_name = item

        # If the wavelength has no unit, there is a problem in the file !
        if data[wave_name].unit is None:
            LOG.error('------------------> Failure :: No wavelength unit, please check source file format')
            LOG.error('                               File :{} shall be in ECSV0.9 (from astropy-2.0)'.format(fileName))

        self.wavelength = data[wave_name].data * data[wave_name].unit

        value = data[data_name].data
        unitString = data[data_name].unit

        # We define specifically dimensionless unit because astropy file format can't handle it
        if unitString is None:
            unitString = u.dimensionless_unscaled

        self.data = value * u.Unit(unitString)

        return data_name

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END
    def __read_fits(self, fileName):
        """
        . Function to read the variable if it comes from a FITS file. Only takes |.fits

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • fileName      : String - Name of the entry file to read

        >>>> RETURNS
        • data_name     : String - Name of the column in the file
        """
        # print('readfits')
        hdulist = fits.open(fileName)
        self.parameters = hdulist[1].data.names

        # We must have a WAVELENGTH column, and select the other column as the "data" one
        if len(self.parameters) != 2:
            raise ValueError(
                "In the FITS file, we want 2 columns (one named 'WAVELENGTH' and another with a random name")

        wave_name = 'WAVELENGTH'
        data_name = ''
        for item in self.parameters:
            if item != wave_name:
                data_name = item

        data = hdulist[1].data[data_name]
        dataUnitString = hdulist[1].columns[data_name].unit
        self.data = data * u.Unit(dataUnitString)

        wavelength = hdulist[1].data[wave_name]
        wavelengthUnitString = hdulist[1].columns[wave_name].unit
        self.wavelength = wavelength * u.Unit(wavelengthUnitString)

        hdulist.close()

        return data_name

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END
    def getInterpolated(self, wavelength):
        """
        . Function to get the interpolated array over a new wavelength range.

        . !! No extrapolation available.

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • wavelength  : Quantity - wavelength array - monitored by decorator

        >>>> RETURNS
        • data        : Quantity - data array
        """

        waves = wavelength.to(self.wavelength.unit)

        attributeInterpolated = self.interpolator(waves.value)
        return attributeInterpolated * self.data.unit

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END
    def convert_to(self, data_unit, wave_unit, is_flux=False):
        """
        . Function to convert_to data and wavelength to new units. If nothing specified, convert_to data to mJy and/or
        wavelength to u.micron

        . It can manage the conversion Fν.dν to Fλ.dλ

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • data_unit  : New unit for data u.'unit' - unit from astropy
        • wave_unit  : New wavelength unit u.'unit' - unit from astropy
        • [is_flux]  : Boolean - Flag for specific spectral density conversions [optional][default=False]
                                 (ie : not needed for Jy to mJy conversions)
        • [verbose]  : Boolean - To enable prints [optional][default=False]

        >>>> RETURNS
        /no returns/
        """

        old_wave_unit = self.wavelength.unit
        old_data_unit = self.data.unit
        self.wavelength = self.wavelength.to(wave_unit)

        LOG.debug("           . Convert: wavelength unit from {} to {}".format(old_wave_unit, self.wavelength.unit))
        #
        # LOG.debug(self.data)

        if is_flux:
            self.data = self.data.to(data_unit, equivalencies=u.spectral_density(self.wavelength))
        else:
            self.data = self.data.to(data_unit)

        LOG.debug("           . Convert: data unit from {} to {}".format(old_data_unit, self.data.unit))
        # LOG.debug(self.data)

        self.interpolator = interp1d(self.wavelength.value, self.data.value)

        return

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END
    def check(self):
        """
        . Function to check the validity of a spectrum. Mainly used for test and developpement purposes.

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • [verbose]         : Boolean - To enable prints [optional][default=False]

        >>>> RETURNS
        /no returns/
        """
        message = ''
        status = True

        if self.wavelength.min() < 0:
            message += 'wavelength negative\n'
            status = False

        if self.wavelength.min() * self.wavelengthUnit < 1 * u.um:
            message += 'Warning: min(wavelength) < 1 micron\n'
            # status = False

        if self.wavelength.max() * self.wavelengthUnit > 100 * u.um:
            message += 'Warning: max(wavelength) > 100 micron\n'
            # status = False

        if message:
            LOG.info(message)
        else:
            LOG.info("OK")

        return status

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END
    def plot(self):
        """
        . Function to check plot a spectrum. Mainly used for test and developpement purposes.

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • [id]  : Integer - to enable multiple figures [optional][default=1]

        >>>> RETURNS
        /no returns/
        """
        plt.figure()
        plt.plot(self.wavelength, self.data)
        plt.xlabel('Wavelelength [{}]'.format(self.wavelength.unit.to_string()))
        plt.ylabel('Data [{}]'.format(self.data.unit.to_string()))
        plt.show()
        return

    def compare(self, spectrum, rtol=1e-7):
        """
        . Function to compare two spectra. Mainly used for test and developpement purposes.

        • Simplified version that suppose units are the same, doesn't convert yet if necessary.

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • spectrum : WavelengthDepdtVariable - external spectrum to compare to self.data
        • [id]     : Integer - to enable multiple figures [optional][default=1]

        >>>> RETURNS
        • status1  : Boolean - verification data values are the same
        • status2  : Boolean - verification wavelength values are the same
        • status3  : Boolean - verification data units are the same
        • status4  : Boolean - verification wavelength units are the same
        """
        # np.equal(wavelength,wavelength2) works
        # np.isclose(wavelength,wavelength2) does not work
        # np.allclose   does not work
        status1 = np.allclose(self.data.value, spectrum.data.value, rtol=rtol)
        status2 = np.allclose(self.wavelength.value, spectrum.wavelength.value, rtol=rtol)
        status3 = self.data.unit == spectrum.data.unit
        status4 = self.wavelength.unit == spectrum.wavelength.unit
        return status1 and status2 and status3 and status4

    def writeAscii(self, fileName, overwrite=True, history=None):
        """
        . Function to write the variable in an ASCII file

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • fileName      : String - Name of the entry file to read
        • [overwrite]   : Boolean - To allow or not to write over an existing file [optional][default=True]
        • [history]     : Status - To enable the mention of the date in the file [optional][default=None]

        >>>> RETURNS
        /no returns/
        """
        col1 = Column(self.wavelength, name='wavelength', unit=self.wavelength.unit.to_string(), dtype='float32')
        col2 = Column(self.data, name='data', unit=self.data.unit.to_string(), dtype='float32')

        t = Table([col1, col2])
        t.write(fileName, format='ascii.ecsv', overwrite=overwrite)
        ff = open(fileName, 'a')
        ff.write('# DATE = ' + str(datetime.datetime.now()) + '\n')
        if history is not None:
            ff.write('# HISTORY = ' + str(datetime.datetime.now()) + '\n')
        ff.close()
        return 0

    def writeFits(self, fileName, overwrite=True, history=None):
        """
        . Function to write the variable in an FITS file

        . Developped for transitGenerator project
        (Creation 2018/02/13 (RG) -- Last update 2018/02/15 (MML))

        >>>> PARAMETERS
        • fileName      : String - Name of the entry file to read
        • [overwrite]   : Boolean - To allow or not to write over an existing file [optional][default=True]
        • [history]     : Status - To enable the mention of the date in the file [optional][default=None]

        >>>> RETURNS
        /no returns/
        """
        col1 = fits.Column(name='wavelength', format='E', unit=self.wavelength.unit.to_string(),
                           array=self.wavelength.value)
        col2 = fits.Column(name='data', format='E', unit=self.data.unit.to_string(), array=self.data.value)

        hdu = fits.BinTableHDU.from_columns([col1, col2])
        if history is not None:
            hdu.header.append(('HISTORY', history), end=True)
        hdu.header['DATE'] = str(datetime.datetime.now())
        hdu.writeto(fileName, overwrite=overwrite)
        return 0

    def __str__(self):
        """
        when you type >> print(my_object)
        __str__ goal is to be readable
        """
        sb = []
        for key in self.__dict__:
            sb.append("    • {key}='{value}'\n".format(key=key, value=self.__dict__[key]))
        return '\n'.join(sb)

    def __repr__(self):
        """
        when you just type >> my_object
        __repr__ goal is to be unambiguous
        """
        # return self.__str__()
        return "%s(%r)" % (self.__class__, self.__dict__)


def geometryTransition(z, p):
    """
    This module is dedicated to the calculation of the transition geometry, when the planet enter or exit the
    stellar disc, therefore one of the bodies is partially obstructed. (Mandel and Algol 2002)

    . This module doesn't consider limb darkening. Used independantly, the present algorithm implicitely induce a local homogeneous disc around the planet.

    . Mathematically, the present function compute the common area between the two circles (occulted) and is
    normalized over the area of the star, and takes the opposite area (transmitted)

    (Creation 2017/11/16 -- Last update 2019/09/17 (CC))



    >>>> PARAMETERS
        • z   : float (not a quantity) - Distance between the center of the star disc and the center of the planet
                                       disc [Rstar]
        • p        : float (not a quantity) - Ratio between the radius of the planet and the radius of the star


    >>>> RETURNS
        • transmission       : float (not a quantity) - Ratio of the area of the star visible from Earth. (technically dimensionless)
                                            • Shall be between (1 - p^2)
    """
    p2 = p ** 2
    z2 = z ** 2
    cosAngle1 = (1 - p2 + z2) / (2 * z)
    cosAngle2 = (p2 + z2 - 1) / (2 * p * z)
    B = (4 * z2 - (1 + z2 - p2) ** 2) / 4

    # Is there intersection between the solar and planet circles?
    selection = (abs(cosAngle1) <= 1.) & (abs(cosAngle2) <= 1.) & (B >= 0.)

    # When transit has not started (planet outside solar surface, transmission is 1)
    transmission = np.ones_like(cosAngle1)

    # No intersection but planet completely inside solar surface
    full_transit = ~selection & (z < 1.)
    transmission[full_transit] = 1. - p2[full_transit]

    # Lunula calculation (in transit, but not fully inside)
    kappa1 = np.arccos(cosAngle1[selection])
    kappa0 = np.arccos(cosAngle2[selection])

    A = np.sqrt(B[selection])
    # print(np.size(p2[selection]), np.size(kappa0), np.size(kappa1), np.size(A))
    occultation = (1 / pi) * (p2[selection] * kappa0 + kappa1 - A)
    transmission[selection] = 1. - occultation

    return transmission  # END


def phaseComputation(time, phaseShift, planetFactor):
    """
    This module is dedicated to the calculation of the phase of the planet, eventally shifted due to atmospherical
    circulation.

    (Creation 2018/02/22 (MML) -- Last update *NoUpdate*)



    >>>> PARAMETERS
        • time             : quantity - angle (0 at mid-transit) [rad]
        • phaseShift       : quantity - Shift of the phase of the planet [rad]
        • planetFactor     : float - Portion of the area of planet visible (between 0 and 1)


    >>>> RETURNS
        • planetDayPhase   : float - Section of the area of the star visible from Earth.
                                            • Shall be between (1 - sizeRatio^2)
    """
    # From "The Exoplanet Handbook" of 2011 p.124 : the portion of stellar tmp catched by the planet :
    #               !! Assuming that planetRadius << starRadius << semiMajorAxis !!
    planetDayPhase = planetFactor * ((1. - np.cos(time + phaseShift)) / 2.)
    planetNightPhase = planetFactor - planetDayPhase

    return planetDayPhase, planetNightPhase  # END


def SEDfileCreation(wl, spectrum, fileName, verbose=False):
    """
    Module dedicated to the redaction of the output file interfaced with MIRISIM.

    (Creation 2017/10/16 -- Last update 2017/12/21 (MML))


    >>>> PARAMETERS
        • wl       : 1D np.array - wavelengths in [m]
        • spectrum : 1D np.array - flux in [μJy]
        • fileName : string - complete name of the SED file to be created


    >>>> RETURNS
        /no returns/
    """
    # CONVERSIONS - MIRISIM UNITS (μm / μJy)
    LOG.debug('>> SED file creation')
    LOG.debug('     . Writing file : {}\n'.format(fileName))
    res = len(wl)
    with open(fileName, "w") as file:
        strVersion = '#         V{} - MML (CEA-Saclay)\n'.format(__version__)
        strDate = '#       {}\n'.format(now)
        file.write('# %ECSV 0.9\n')
        file.write('# ---\n')
        file.write('# datatype:\n')
        file.write('# - {name: wavelength, unit: micron, datatype: float64}\n')
        file.write('# - {name: flux, unit: microJansky, datatype: float64}\n')
        file.write('# schema: astropy-2.0\n')
        file.write('#\n#\n')
        file.write('#  - - File generated by ExoNoodle - -\n')
        file.write(strVersion)
        file.write(strDate)
        file.write('wavelength flux \n')
        spectrum = spectrum.to(u.uJy)
        for i in range(res):
            strValues = '{0:9.5E}        {1:15.11E}\n'.format(wl.value[i], spectrum.value[i])
            file.write(strValues)

    return  # END


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

def conservative_interpolation(in_waves, in_data, out_waves):
    """
    Returns flux obtained from integrating the flux density, iterpolating on wave and derivating back to flux.

    :param numpy.ndarray in_waves:  .Original wavelengths of in_data
    :param numpy.ndarray in_data:   .Values to be interpolated on target wavelengths
    :param numpy.ndarray out_waves: .Wavelength on which in_data has to be interpolated

    :return: specflux              .Values interpolated
    """
    # Calculatine mean step to add values to wavelength
    # We assume out_waves is a regular grid
    step_out_waves = (np.min(out_waves) - np.max(out_waves)) / (np.size(out_waves))
    # Integration
    data_index = cumulative_integration(in_data, in_waves)
    # Adding a tmp prior to the first step
    primitive = np.concatenate([np.array([0.]), data_index])
    # Interpolation
    interpolator = interp1d(in_waves, primitive, kind='cubic')
    # We need a check that doesn't allow for extrapolation
    if np.min(out_waves - step_out_waves / 2.) < np.min(in_waves):
        LOG.warning("------------------> Warning :: In interpolation, you were trying to extrapolate (too small)")
    elif np.max(out_waves + step_out_waves / 2.) > np.max(in_waves):
        LOG.warning("------------------> Warning :: In interpolation, you were trying to extrapolate (too large)")
    # else:
    data1 = interpolator(out_waves - step_out_waves / 2.)
    data2 = interpolator(out_waves + step_out_waves / 2.)

    # differenciation
    out_data = (data2 - data1) / step_out_waves
    return out_data


def is_all_equal(array):
    """
    Function to check that all tmp in a vector are the same.

    :param numpy.dnarray array: array to check.

    :return bool: boolean result. True or False
    """

    if len(array) > 0:
        bool = all(elem == array[0] for elem in array)

    return bool


def times_file_creation(times, time_in_seconds, padding, output_dir):
    """
    time_in_seconds is the time since first observation. Hence, the first tmp must always be 0.
    """
    # We create the list of phases corresponding to each future SED
    fileName = os.path.join(output_dir, "times.dat")

    with open(fileName, "w") as file:
        strVersion = '#         V{} - MML (CEA-Saclay)\n'.format(__version__)
        strDate = '#       {}\n'.format(now)
        col_title = 'file_index phase time\n'
        file.write('# %ECSV 0.9\n')
        file.write('# ---\n')
        file.write('# datatype:\n')
        file.write('# - {name: file_index, unit: '', datatype: float64}\n')
        file.write('# - {name: phase, unit: '', datatype: float64}\n')
        file.write('# - {name: time, unit: second, datatype: float64}\n')
        file.write('# schema: astropy-2.0\n')
        file.write('#\n#\n')
        file.write('#  - - File generated by exoNoodle - -\n')
        file.write(strVersion)
        file.write(strDate)
        file.write(col_title)

        # Time in radian, Time in seconds
        for i, (tir, tis) in enumerate(zip(times, time_in_seconds)):
            file.write("{:0{}d}   {}   {}\n".format(i, padding, tir.value/(2 * np.pi), tis.value))

        file.close()
        print('\n')

        return 0


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def progressBar(value, endvalue, message, bar_length=20):
    percent = float(value) / endvalue
    arrow = '-' * int(round(percent * bar_length) - 1) + 'x'
    spaces = ' ' * (bar_length - len(arrow))

    sys.stdout.write("\r{0} [{1}] {2}%".format(message, arrow + spaces, int(round(percent * 100))))
    sys.stdout.flush()

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> END
