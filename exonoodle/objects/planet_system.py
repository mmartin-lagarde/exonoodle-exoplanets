#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                    exoNoodle : Instantaneous spectrum                     #
#                          -- Planetary System --                           #
# --------------------------------------------------------------------------#
#
#
# CONTRUBUTORS :
# --------------
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#
# Part of exoNoodle package
# Main : system_instant_spectrum() in compute.py
# (TBI : To be integrated)
# All units are astropy.units
#
# Developped in Python 3
# Suitable for Python 2.7
#
#
#  CONTAINS :
# -----------
#   class SystemContribution
#        init, orbitalMechanics, spectrum, setCenterDistance, plainTransit, transit_LimbDarkening
#        limbDarkeningComputation, limitTransit, transitionPhase, limitEclipse, plainEclipse, startTransit
#        check_overlap

from .. import utils
import math
import numpy as np
from astropy import units as u
from astropy import constants as c
from pdb import set_trace as stop

import logging
LOG = logging.getLogger('exonoodle.planet_system')

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#        _____                 .
#       (      ,    .    ____ _/_     ___  , _ , _          ___  , _ , _   `   ____   ____ `   __.  , __
#        `--.  |    `   (      |    .'   ` |' `|' `.      .'   ` |' `|' `. |  (      (     | .'   \ |'  `.
#           |  |    |   `--.   |    |----' |   |   |      |----' |   |   | |  `--.   `--.  | |    | |    |
#      \___.'   `---|. \___.'  \__/ `.___, /   '   /      `.___, /   '   / / \___.' \___.' /  `._.' /    |
#               \___/
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# Class describing the planet transmission contribution.
#
#
# Reads the entry files : configuration.ini
#                         StarSpectrum|.txt
#                                     |.dat
#                                     |.fits
class SystemContribution(object):
    """
    Class storing the emission of the system as a whole. Informations from Star and Planet objects.

    >>>> CHARACTERISTICS
    > from __init__:
    • name
    • star   : type StarContribution
    • planet : type PlanetContribution
    • impactFactor
    • semiMajorAxis
    • T0Transit



    >>>> METHODS
    • spectrum(self, time, verbose=False) - ** CORE CALCULATION **
    • __orbitalMechanics(self, verbose=False)
    • setCenterDistance(self, time, verbose=False)
    • plainTransit(self, time, verbose=False)
    • exitTransit(self, time, verbose=False)
    • transitionPhase(self, time, verbose=False)
    • startEclipse(self, time, verbose=False)
    • plainEclipse(self, time, verbose=False)
    • exitEclipse(self, time, verbose=False)
    • startTransit(self, time, verbose=False)
    • check_overlap(self, planetRadius_i, centerDistance, verbose=False)




    . Developped for transitGenerator project
    (Creation 2018/01/09 (MML) -- Last update 2018/02/05 (MML))

    """

    # http://docs.astropy.org/en/stable/units/quantity.html
    # decorator to check the type of the argument
    @u.quantity_input(time='angle')
    def __init__(self, star, planet):
        LOG.debug('>> System Contribution Creation (with star {} and planet {})'.format(star.name, planet.name))
        self.name = star.name[:-1]
        LOG.debug('        > System {}'.format(self.name))
        self.impactFactor = planet.semiMajorAxis * np.cos(planet.orbitalInclination) / star.radius
        self.semiMajorAxis = planet.semiMajorAxis
        # self.T0Transit = planet.t0Transit
        self.star = star
        self.planet = planet
        # ORBITAL MECHANICS CONSTANTS OF THE SYSTEM
        self.__orbitalMechanics()

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> SPECTRUM COMPILATION
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # This module will g$
    # transit.
    # Takes into account :
    #               - Limb darkening (TBI)
    #               - Light fluctuation (TBI)
    #               - Elliptical orbit (TBI)
    #               - Various number of planets in the system (TBI)
    #
    #
    # Reads the entry files : configuration.ini
    def spectrum(self, time, verbose=False):
        """
        . Module to compile the different spectra involved in the system emission.
        Orbital mechanics is re-calculated from the system characteristics file.


        (Creation 2017/10/25 (MML) -- Last update 2018/01/15 (MML))


        >>>> PARAMETERS
            • spectrum_star : 1D np.array - flux in [μJy]
            • fileName      : string - complete name of the configuration file
            • time          : quantity - transit phase at the time of the image [rad]


        >>>> RETURNS
            • spectrum      : 1D np.array - flux in [μJy]
        """

        # ORBITAL MECHANICS TIME VARIABLES
        # now_ProjectedOrbitalSpeed = abs(self.OrbitalSpeed * np.cos(time - pi/2)) # Time rescale cos(0) when planet facing.

        # if verbose: print('        > Transit',self.transitPhaseDuration/(2*pi),'rad/2pi')

        # TIMELINE DEFINITION (NO UNIT, RELATIVE PHASE BETWEEN 0 and 2π)
        # Here in chronoligical order (begining of primary transit last).
        self.t_0P = 0. * u.rad  # Mid-transit time (primary) / 0 by definition
        # if verbose: print '          :: 0 transit primary       ', t_0P/(2*pi)
        self.t_EPT_in = self.t_0P + (1. / 2) * self.transitPhaseDuration_in  # End of primary transit
        self.t_EPT_out = self.t_0P + (1. / 2) * self.transitPhaseDuration_out  # End of primary transit
        # if verbose: print '          :: end transit primary     ', t_EPT/(2*pi)
        self.t_0S = math.pi * u.rad  # Mid-transit time (secondary) / π by definition
        # if verbose: print '          :: 0 transit secondary     ', t_0S/(2*pi)
        self.t_SST_out = self.t_0S - (1. / 2) * self.transitPhaseDuration_out  # Start of secondary transit
        self.t_SST_in = self.t_0S - (1. / 2) * self.transitPhaseDuration_in  # Start of secondary transit
        # if verbose: print '          :: start transit secondary ', t_SST/(2*pi)
        self.t_EST_in = self.t_0S + (1. / 2) * self.transitPhaseDuration_in  # End of secondary transit
        self.t_EST_out = self.t_0S + (1. / 2) * self.transitPhaseDuration_out  # End of secondary transit
        # if verbose: print '          :: end transit secondary   ', t_EST/(2*pi)
        self.t_SPT_out = self.t_0P + 2. * math.pi * u.rad - (
                                                                1. / 2) * self.transitPhaseDuration_out  # Start of primary transit
        self.t_SPT_in = self.t_0P + 2. * math.pi * u.rad - (
                                                               1. / 2) * self.transitPhaseDuration_in  # Start of primary transit
        #
        # print("                        *** TIMELINE ***")
        # print("t_EPT_in ", self.t_EPT_in/(2*pi))
        # print("t_EPT_out", self.t_EPT_out/(2*pi))
        # print("t_SST_out", self.t_SST_out)
        # print("t_SST_in ", self.t_SST_in)
        # print("t_0S     ", self.t_0S)
        # print("t_EST_in ", self.t_EST_in)
        # print("t_EST_out", self.t_EST_out)
        # print("t_SPT_out", self.t_SPT_out)
        # print("t_SPT_in ", self.t_SPT_in)
        # print(time, self.t_0P, self.t_EPT_in)

        # Create delayed time (add the light-travel delay)
        # Note that the delay is arbitrary considered NULL at t=0, hence the maximum delay of 2a/c for secondary transit
        # This time is shifted because the light you see at t, is a light emitted at a prior time depending on the
        # extra distance the light had to travel on the line of sight
        delay = (self.semiMajorAxis / c.c) * (np.cos(time) - 1)
        time_delayed = time - utils.time2phase(delay, self.planet.orbitalPeriod)

        # - - - - - - - - - - - - - - - - -  MODULO 2pi COMPUTATION  - - - - - - - - - - - - - - - - - - - -
        # '%' is failing, reason to investigate. Patch with the algorithm
        if time_delayed < self.t_0P:
            timeRescaled = time_delayed
            while timeRescaled < self.t_0P:
                timeRescaled = time_delayed.value + 2 * math.pi
                timeRescaled = timeRescaled * u.rad
            LOG.debug('           ---------> Warning :: Time rescaled to fit between 0 and 2pi')

        elif time_delayed > (2 * math.pi * u.rad):
            timeRescaled = time_delayed
            while timeRescaled > (2 * math.pi * u.rad):
                timeRescaled = time_delayed.value - 2 * math.pi
                timeRescaled = timeRescaled * u.rad
            LOG.debug('           ---------> Warning :: Time rescaled to fit between 0 and 2pi')
        else:
            timeRescaled = time_delayed
                # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            # ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ]
        if  self.impactFactor <= 1.:
            if timeRescaled <= self.t_EPT_in:  # Until the exit of the planet frop the circle
                LOG.debug('         • You are now in transit (After mid-transit time)')
                if self.star.LimbDarkening:
                    self.starFactor, self.planetFactor = self.transit_LimbDarkening(timeRescaled, verbose)
                else:
                    self.starFactor, self.planetFactor = self.limitTransit(timeRescaled, verbose)

            # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
            elif timeRescaled <= self.t_EPT_out:  # Until the end of the transit (former t_EPT)
                LOG.debug('         • You are now at the end of the transit (After mid-transit time)')
                if self.star.LimbDarkening:
                    self.starFactor, self.planetFactor = self.transit_LimbDarkening(timeRescaled, verbose)
                else:
                    self.starFactor, self.planetFactor = self.limitTransit(timeRescaled, verbose)

            # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
            elif timeRescaled <= self.t_SST_out:  # Until the planet disc touches again the star
                LOG.debug('         • You are now between transit and occultation')
                self.starFactor, self.planetFactor = self.transitionPhase(timeRescaled, verbose)

            # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
            elif timeRescaled <= self.t_SST_in:  # Until the planet is completely occulted by the star
                LOG.debug('         • You are now entering the occultation')
                self.starFactor, self.planetFactor = self.limitEclipse(timeRescaled, verbose)

            # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
            elif timeRescaled <= self.t_EST_in: #If:  # Until the planet start sticking out of the star disc
                LOG.debug('         • You are now in occultation')
                self.starFactor, self.planetFactor = self.plainEclipse(timeRescaled, verbose)

            # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
            elif timeRescaled <= self.t_EST_out:  # Until the planet is completely outside the star disc
                LOG.debug('         • You are now at the end of the occultation')
                self.starFactor, self.planetFactor = self.limitEclipse(timeRescaled, verbose)

            # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
            elif timeRescaled <= self.t_SPT_out:  # Until the planet start touching the disc of the star
                LOG.debug('         • You are now between occultation and transit')
                self.starFactor, self.planetFactor = self.transitionPhase(timeRescaled, verbose)

            # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
            elif timeRescaled <= self.t_SPT_in:  # Until the planet is completely over the star disc
                LOG.debug('         • You are now entering the transit (Before mid-transit time)')
                if self.star.LimbDarkening:
                    self.starFactor, self.planetFactor = self.transit_LimbDarkening(timeRescaled, verbose)
                else:
                    self.starFactor, self.planetFactor = self.limitTransit(timeRescaled, verbose)

            # > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > > >
            elif timeRescaled <= (2 * math.pi * u.rad):
                LOG.debug('         • You are now in transit (Before mid-transit time)')
                if self.star.LimbDarkening:
                    self.starFactor, self.planetFactor = self.transit_LimbDarkening(timeRescaled, verbose)
                else:
                    self.starFactor, self.planetFactor = self.limitTransit(timeRescaled, verbose)
            # ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ] ]

        else:
            if  self.impactFactor > 1.:
                self.starFactor, self.planetFactor = self.transitionPhase(timeRescaled, verbose)
            else:
                raise ValueError(f"Failure: Time phase ({time}) shall be between 0 and 2pi")

        #stop()
        self.planetDayPhase, self.planetNightPhase = utils.phaseComputation(time_delayed, self.planet.phaseShift,
                                                                            self.planetFactor)
        self.planet.dayFlux = self.planet.dayEmissionFlux + (
            self.planet.albedo * self.planet.dilutionFactor * self.star.flux)
        self.CompiledSpectrum = self.starFactor * self.star.flux \
                                + self.planetDayPhase * self.planet.dayFlux \
                                + self.planetNightPhase * self.planet.nightEmissionFlux

        #print("planet emission", self.planetDayPhase * self.planet.dayFlux[0] + self.planetNightPhase * self.planet.nightEmissionFlux[0])

        return self.CompiledSpectrum

    def __orbitalMechanics(self):
        """
        . Method aiming to calculate all the celestial mechanics parameter of the system. Called in the __init__
        of SystemContribution class.

        . Sets several orbital parameters necessary for the next computations
            - self.OrbitalPeriod
            - self.OrbitalSpeed
            - self.characteristicAngle (in/out)
            - self.transitPhaseDuration (in/out)


        (Creation 2018/02/05 (MML) -- Last update 2018/02/05 (MML))
        """
        # < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < < REAL DISTANCES
        # A first sort is made with the enveloppe radius of the planet np.max(self.planet.radius)
        self.OrbitalPeriod = ((2 * math.pi) / (np.sqrt(c.G * (self.star.mass + self.planet.mass)))) * np.sqrt(
            self.semiMajorAxis ** 3)  # s
        OrbitalLength = 2 * math.pi * self.semiMajorAxis  # m/s
        self.OrbitalSpeed = OrbitalLength / self.OrbitalPeriod

        # From Stevenson 2017 and Morello thesis (See Rene Gastaud Back to Basics file.)
        x1 = (np.max(self.planet.radius) + self.star.radius) ** 2 - (self.planet.semiMajorAxis * np.cos(
            self.planet.orbitalInclination)) ** 2
        yy = (self.planet.semiMajorAxis * np.sin(self.planet.orbitalInclination)) ** 2
        angle1 = np.arcsin(np.sqrt(x1 / yy))

        x2 = (np.max(self.planet.radius) - self.star.radius) ** 2 - (self.planet.semiMajorAxis * np.cos(
            self.planet.orbitalInclination)) ** 2
        angle2 = np.arcsin(np.sqrt(x2 / yy))

        self.transitPhaseDuration_out = 2 * angle1
        self.transitPhaseDuration_in = 2 * angle2

        return

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def setCenterDistance(self, time):
        """
        . Method aiming to calculate the centerDistance at a given time

        . Geometrically, the problem is to solve the radius of an ellipse at a given angle
            - a is the semi-major axis
            - b is the semi-minor axis
            - theta is the angle (0 for max radius : a)


        (Creation 2018/02/05 (MML) -- Last update 2018/02/05 (MML))


        >>>> PARAMETERS
            • self          : SystemContribution class - system object
            • time          : Quantity - angle (0 at mid-transit) [rad]
            • verbose       : Boolean


        >>>> RETURNS
            • centerDistance : quantity - distance between the center of the two circles
        """

        a = self.semiMajorAxis
        b = self.impactFactor * self.star.radius
        theta = time + ((math.pi / 2.) * u.rad)
        centerDistance = np.sqrt((a * np.cos(theta)) ** 2 + (b * np.sin(theta)) ** 2)
        return centerDistance

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def plainTransit(self, time, verbose=False):
        LOG.debug('         ... STAR WEAKED + PLANET TRANSMISSION + PLANET EMISSION')
        transmission = 1 - self.planet.absorption

        starFactor = transmission
        planetFactor = 1.

        return starFactor, planetFactor

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def transit_LimbDarkening(self, time, verbose=False):
        LOG.debug('         ... STAR WEAKED (LIMB DARKENING) + PLANET TRANSMISSION + PLANET EMISSION +  EFFECT')
        # OCCULTATION FACTOR FOR TRANSIT DEPTH                  # p in Mandel et al. 2002
        centerDistance = self.setCenterDistance(time)

        obstructedFlux = self.limbDarkeningComputation(centerDistance, time)
        transmission = 1. - obstructedFlux

        starFactor = transmission
        planetFactor = 1.

        return starFactor, planetFactor

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def limbDarkeningComputation(self, centerDistance, time, verbose=False, nbMeshPoints=3000):
        """
            . Method computing the portion of the star flux occulted by the planet, considering limb
            darkening.


            >>>> PARAMETERS
            • self          : SystemContribution class - system object
            • centerDistance : quantity - distance between the center of the star and the center of the planet  [m]
            • verbose       : Boolean
            • nbMeshPoints : number of points in the grid


            >>>> RETURNS
            • obstructedFlux : quantity without unit, same shape than self.star.wavelength (<==> self.planet.radius)
            (ratio of the obstructed flux to the absolute star flux)


        """

        # We copy values without quantities to be faster
        planet_radius = self.planet.radius.value
        star_radius = self.star.radius.value

        ray_min = centerDistance.value - planet_radius
        ray_max = centerDistance.value + planet_radius
        ray_max = np.clip(ray_max, 0, star_radius)

        meshStep = (ray_max - ray_min) / nbMeshPoints
        obstructedFlux = 0.
        intersectingSurface = 0.
        Slunula_n = np.zeros_like(self.star.wavelength.value)

        for n in range(nbMeshPoints):
            ray = ray_min + n*meshStep

            # From now on, for calculations details, please refer to
            #   the 2nd thesis report of March 2018 (Martin-Lagarde) sect. 2.6
            ray_StellarRadius = ray / star_radius
            # - - - - - - - - - - - -  GAMMA / RADIUS RELATION - - - - - - - - - - - - - - -  (28)
            selection = (ray_StellarRadius <= 1.)
            gamma = np.arcsin(ray_StellarRadius[selection])
            # - - - - - - - - - - - - - - - INPUTS FOR LUNULA - - - - - - - - - - - - - - - -
            #           Considering computation with major (star) radius as ray[n]
            relativeDistance_np1 = centerDistance.value / (ray + meshStep)
            sizeRatio_np1 = planet_radius / (ray + meshStep)
            # - - - - - - - - - - - - - - -  LUNULA AND Sn  - - - - - - - - - - - - - - - - - (1)
            #Slunula_n = Slunula_np1
            #Slunula_n =   (1. - utils.geometryTransition(relativeDistance_n  , sizeRatio_n))   * (ray              / self.star.radius) ** 2
            if np.isclose(((ray[0] + meshStep[0]) - centerDistance.value + planet_radius[0])/(2*planet_radius[0]), 1., atol=1e-10, equal_nan=False):
                Slunula_np1 = (planet_radius/star_radius)**2
            else:
                Slunula_np1 = (1. - utils.geometryTransition(relativeDistance_np1, sizeRatio_np1)) \
                              * ((ray + meshStep) / star_radius) ** 2

            S_n = Slunula_np1 - Slunula_n
            # - - - - - - - - - - - - - LIMB DARKENING FORMULA - - - - - - - - - - - - - - - (27)
            lightRatio = np.zeros_like(S_n)
            lightRatio[selection] = self.star.LimbDarkening.compute(gamma, selectionOK=selection)
            lightRatio[~selection] = 1.
            # - - - - - - - - - - - -  OBSTRUCTED FLUX FORMULA  - - - - - - - - - - - - - - - (29)
            obstructedFlux = obstructedFlux + lightRatio * S_n  # S
            intersectingSurface = (intersectingSurface + S_n)

            # print("Surface", intersectingSurface[0]/(self.planet.radius[0]/self.star.radius)**2, \
            #       "| Lunula (n)", Slunula_n[0]/(self.planet.radius[0]/self.star.radius)**2, \
            #       "| Lunula (n+1)", Slunula_np1[0]/(self.planet.radius[0]/self.star.radius)**2, \
            #       "| Ray (n+1)", ((ray[0] + meshStep[0]) - centerDistance + self.planet.radius[0])/(2*self.planet.radius[0]))

            Slunula_n = Slunula_np1

        # print("time :", time, \
        #        "\nObstructed Flux ::", (self.planet.radius[0]/self.star.radius)**2 == obstructedFlux[0], \
        #        " | Radius ::", (ray[0]+meshStep[0])/(2*self.planet.radius[0]) == (ray_min[0] + nbMeshPoints*meshStep[0])/(2*self.planet.radius[0]))

        return obstructedFlux

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def limitTransit(self, time, verbose=False):
        centerDistance = self.setCenterDistance(time)
#       Add a test to create an array of one item if it's a single radius...

        transmission = np.zeros(len(self.planet.radius))

        for i in range(len(self.planet.radius)):
            # print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
            # print(i)
            overlap = self.check_overlap(self.planet.radius[i], centerDistance)

            if overlap == 'inside':
                # print("overlapStatus: ", overlap)
                transmission[i] = 1 - self.planet.absorption[i]
                planetFactor = 1.

            elif overlap == 'outside':
                # print("overlapStatus: ", overlap)
                transmission[i], planetFactor = self.transitionPhase(time, verbose=False)

            elif overlap == 'overlap':
                sizeRatio = self.planet.radius[i] / self.star.radius  # p in Mandel et al. 2002
                relativeDistance = centerDistance / self.star.radius  # z in Mandel et al. 2002
                transmission[i] = utils.geometryTransition(relativeDistance.value, sizeRatio.value)
                planetFactor = 1.
                # print("overlap: ", overlap, ">> geometryTransition gives",transmission[i])
        LOG.debug('         ... STAR WEAKED IN TRANSITION + PLANET TRANSMISSION + PLANET EMISSION')  # PLANET NIGHT EMISSION'
        starFactor = transmission

        return starFactor, planetFactor

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def transitionPhase(self, time, verbose=False):
        LOG.debug('         ... STAR ALONE + PLANET EMISSION')  # + PLANET EMISSION (DAY APPEARENCE)'
        starFactor = 1.
        planetFactor = 1.
        return starFactor, planetFactor

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def limitEclipse(self, time, verbose=False):
        centerDistance = self.setCenterDistance(time)

        sizeRatio = self.planet.radius / self.star.radius  # p in Mandel et al. 2002
        relativeDistance = centerDistance / self.star.radius  # z in Mandel et al. 2002
        # Details of the folowing line formula is explained in the documentation.
        transmission = 1. - (1 - utils.geometryTransition(relativeDistance.value, sizeRatio.value)) * \
                            (self.star.radius ** 2) / (self.planet.radius ** 2)
        # print(transmission[i])
        starFactor = 1.
        # print("overlap: ", overlap, ">> geometryTransition gives",transmission[i])
        LOG.debug(
                '         ... PLANET WEAKED IN TRANSITION + PLANET TRANSMISSION + PLANET EMISSION')  # PLANET NIGHT EMISSION'
        # print("  > ", overlap)
        planetFactor = transmission

        return starFactor, planetFactor

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def plainEclipse(self, time, verbose=False):
        LOG.debug('         ... STAR ALONE')
        transmission = 0.
        starFactor = 1.
        planetFactor = transmission
        return starFactor, planetFactor

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def startTransit(self, time, verbose=False):
        centerDistance = self.setCenterDistance(time)
        transmission = np.zeros(len(self.planet.radius))

        for i in range(len(self.planet.radius)):
            # print("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
            # print(i)
            overlap = self.check_overlap(self.planet.radius[i], centerDistance)

            if overlap == 'inside':
                # print("overlapStatus: ", overlap)
                transmission[i] = 1 - self.planet.absorption[i]
                planetFactor = 1.

            elif overlap == 'outside':
                # print("overlapStatus: ", overlap)
                transmission[i], planetFactor = self.transitionPhase(time, verbose=False)

            elif overlap == 'overlap':
                sizeRatio = self.planet.radius[i] / self.star.radius  # p in Mandel et al. 2002
                relativeDistance = centerDistance / self.star.radius  # z in Mandel et al. 2002
                transmission[i] = utils.geometryTransition(relativeDistance.value, sizeRatio.value)
                planetFactor = 1.
                # print("overlap: ", overlap, ">> geometryTransition gives",transmission[i])
        LOG.debug(
            '         ... STAR WEAKED IN TRANSITION + PLANET TRANSMISSION + PLANET EMISSION')  # PLANET NIGHT EMISSION'
        starFactor = transmission

        return starFactor, planetFactor

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    def check_overlap(self, planetRadius_i, centerDistance):
        """
        . Method aiming to detect the status of the overlapping between the projected star and planet circles.


        (Creation 2017/02/01 (MML) -- Last update 2018/02/05 (MML))


        >>>> PARAMETERS
            • self            : SystemContribution class - system object
            • planetRadius_i  : quantity - radius of the planet at the ith wavelength
            • centerDistance  : quantity - center-to-center distance between the two discs
            • verbose         : Boolean


        >>>> RETURNS
            • overlap         : string - set values of the diagnosis
                                -> 'overlap' tmp if the circles are partly overlapping
                                -> 'inside'  tmp if the planet is compelety inside the star
                                -> 'outside' tmp if the planet is completely outside the star
                                -> 'error' if none of the conditions fits
        """
        # when [centerDistance = radius_star - radius_planet]
        #                 the disc of the planet touches the edge of the star
        maximumDistance = self.star.radius + planetRadius_i
        minimumDistance = self.star.radius - planetRadius_i
        if centerDistance < minimumDistance:
            overlap = 'inside'
        elif (centerDistance >= minimumDistance) and (centerDistance <= maximumDistance):
            overlap = 'overlap'
        elif centerDistance > maximumDistance:
            overlap = 'outside'
        else:
            overlap = 'error'
        return overlap
