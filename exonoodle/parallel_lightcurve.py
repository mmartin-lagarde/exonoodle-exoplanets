#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
usefull source: https://stackoverflow.com/questions/5442910/python-multiprocessing-pool-map-for-multiple-arguments
"""

from multiprocessing import Pool
import numpy as np
from astropy import units as u
import os
import time
import shutil
from . import config
from . import compute
from . import utils
from . import constants
from .version import __version__

import logging
LOG = logging.getLogger('exonoodle.parallel_lightcurve')

def parallel_lightcurve(params):
    """
    Launches the exonoodle computation in parallel

    :param params: Complete name of the configuration file or corresponding config obj dictionnary
    :type params: string or ConfigObj
    :param nb_proc: Number of CPU used for the computation
    :type nb_proc: integer
    :param folder: Path to where the results will be stored. Has to be an existing folder
    :type folder: string, default = None (results store in active folder)
    :param time_min: Time start of the light-curve window
    :type time_min: float, default = 0, unit = rad
    :param time_max: Time end of the light-curve window
    :type time_max: float, default = 6.283185307179586 (2*pi), unit = rad

    :return: None
    :rtype: None
    """

    utils.init_log(log="exonoodle.log", stdout_loglevel="INFO", file_loglevel="INFO")

    LOG.info("--------------------------------------------------------------------------------------------------------------------")
    LOG.info("                                                     -   ExoNoodle   -")
    LOG.info("                                                   PARALLEL LIGHTCURVE RUN")
    LOG.info("--------------------------------------------------------------------------------------------------------------------")
    LOG.info(" Version {} - CEA-Saclay\n".format(__version__))
    start_time = time.time()

    if isinstance(params, str):
        configuration = config.read(params)
    else:
        configuration = params

    folder = configuration["exonoodle"]["folder"]
    phase_min = configuration["exonoodle"]["phase_min"]
    phase_max = configuration["exonoodle"]["phase_max"]
    time_min = (2 * np.pi) * phase_min
    time_max = (2 * np.pi) * phase_max
    nb_proc = configuration["exonoodle"]["nb_proc"]

    output_dir = utils.create_directory(folder)
    LOG.info('>> Directory created : {}/'.format(output_dir))



    wavelength = config.init_waves(configuration)

    LOG.debug('   File: {}'.format(params))

    period = configuration["orbit"]["orbital_period"]
    sampling = configuration["exonoodle"]["time_sampling"]
    # If unit is time and not radians, do the conversion
    if sampling.decompose().unit == "s":
        light_curve_sampling = utils.time2phase(sampling, period)
        light_curve_sampling = light_curve_sampling.value

    nb_points = int(np.ceil((time_max - time_min) / light_curve_sampling))
    times = np.linspace(time_min, time_max, nb_points) * u.rad  # radians
    times_in_seconds = (period / (2 * np.pi)) * times
    times_in_seconds -= times_in_seconds[0]  # First time in seconds is always 0
    padding = int(np.log10(nb_points)) + 1



    # We make sure each chunks is smaller than the stellar stability to ensure
    # using a constant stellar spectrum is a valid approximation
    stellar_stability = utils.time2phase(constants.stellar_stability.to(u.s), period)
    chunks_size = int(np.floor(stellar_stability.value / light_curve_sampling))
    utils.times_file_creation(times, times_in_seconds, padding, output_dir)
    LOG.info('>> Time sheet created : {}/times.dat'.format(output_dir))
    LOG.info('>> Noodles start cooking')
    LOG.info('    • CPU used : {}'.format(nb_proc))
    # List of arguments for each instance of system_instant_spectrum
    args = []
    nb_chunks = 0
    for i in np.arange(0, nb_points, chunks_size):
        args.append((times[i:i+chunks_size], configuration, output_dir, i, padding, wavelength))
        nb_chunks += 1
    LOG.info('    • Jobs     : {} (with max {} run/job)'.format(nb_chunks, chunks_size))

    p = Pool(processes=nb_proc)
    results = p.starmap(compute.phase_system_spectrum, args)
    LOG.info(' Done with success, files {}/SED_index.dat'.format(output_dir))

    # >>>>> END PRINTS
    end_time = time.time()

    output_filename = os.path.join(output_dir, os.path.basename(configuration.filename))
    shutil.copy(configuration.filename, output_filename)

    LOG.info("\nexoNoodle run: {:.2g} m".format((end_time - start_time)/60.))
    LOG.info("                                                                                    *** exoNoodle ***   End")
