#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Marine Martin-Lagarde (CEA-Saclay)  marine.martin-lagarde@cea.fr
#    René Gastaud (CEA-Saclay)          rene.gastaug@cea.fr
#    4 February 2018
#
#   To launch type >>> py.test test_simple_spectrum.py
#   Create  dumy fits and dumm ascii file

import os
import numpy as np
import numpy.testing as npt
# http://docs.astropy.org/en/stable/units/equivalencies.html
from astropy import units as u
from exonoodle.utils import WavelengthDepdtVariable


def test_simple_spectrum():
    """
    Test Simple Spectrum

    :return:
    :rtype:
    """
    fileName = 'xx_spectrum'

    # create an object ex nihilo
    wavelength = (np.arange(10) + 5.) * 1E4
    wavelength_unit = u.AA
    wavelength = wavelength * wavelength_unit
    ref_flux = (np.sin(np.arange(10)) + 3.) * 1E-13
    # Conversion impossible entre ref_flux_unit et Jansky :
    # astropy.units.core.UnitConversionError: 'ph / (cm2 Hz s)' (photon flux density) and 'mJy' (spectral flux density) are not convertible
    # d'où vient ref_flux_unit et comment ça marchéit avant ?
    ref_flux_unit = u.photon / u.cm ** 2 / u.s / u.Hz
    ref_flux = ref_flux * ref_flux_unit

    mySpectrum = WavelengthDepdtVariable(wavelength=wavelength, data=ref_flux)

    #  remark npt.assert works with quantity, even with different units,
    #  works with wavelength.to(u.AA), but not with wavelength.decompose() ??
    npt.assert_array_equal(mySpectrum.data.value, ref_flux.value)
    assert (mySpectrum.data.unit == ref_flux.unit)
    npt.assert_array_equal(mySpectrum.wavelength.value, wavelength.value)
    assert (mySpectrum.wavelength.unit == wavelength.unit)

    #   test the method getInterpolated
    wave_05 = (wavelength.value[0] + wavelength.value[1]) / 2. * u.AA
    ref_flux_05 = (ref_flux.value[0] + ref_flux.value[1]) / 2. * ref_flux_unit
    flux_05 = mySpectrum.getInterpolated(wave_05)
    assert (flux_05.value == ref_flux_05.value)

    #  test getInterpolated with an input wavelength with different unit
    wave_05 = wave_05.to(u.micron)
    flux_05 = mySpectrum.getInterpolated(wave_05)
    assert (flux_05.value == ref_flux_05.value)

    #  check  method compare
    assert (mySpectrum.compare(mySpectrum))

    #  does not exist   mySpectrum2 = mySpectrum.copy()
    #  beware wavelength, data are object and are not copied in the the object WavelengthDepdtVariable, but referenced
    #  shallow and deep copy
    mySpectrum2 = WavelengthDepdtVariable(wavelength=wavelength.copy(), data=ref_flux.copy())
    assert (mySpectrum.compare(mySpectrum2))

    wavelength2 = wavelength.to(u.micron)
    # check that wavelength is not modified remember that mySpectrum.wavelength is a reference
    assert (wavelength.unit == wavelength_unit)
    # check that npt.assert_array_equal manages different units
    npt.assert_array_equal(wavelength, wavelength2)
    npt.assert_allclose(wavelength, wavelength2, rtol=1e-18)
    # not work np.allclose(wavelength, wavelength2, rtol=1e-18)

    # check compare  compare does not manage different units
    mySpectrum2.wavelength = mySpectrum2.wavelength.to(u.micron)
    assert (not mySpectrum.compare(mySpectrum2))

    mySpectrum2.wavelength = mySpectrum2.wavelength.to(u.AA)
    assert (mySpectrum.compare(mySpectrum2))
    mySpectrum2.wavelength.value[0] = 50001.
    assert (not (mySpectrum.compare(mySpectrum2)))
    assert (mySpectrum.compare(mySpectrum2, rtol=1e-4))
    assert (not (mySpectrum.compare(mySpectrum2, rtol=1e-5)))

    ##  check read/write fits  and method compare
    mySpectrum.writeFits(fileName + '.fits', history='version 1.3')
    mySpectrum2 = WavelengthDepdtVariable(fileName + '.fits')
    assert (mySpectrum.compare(mySpectrum2))
    ##  check read/write ascii
    mySpectrum.writeAscii(fileName + '.dat')
    mySpectrum3 = WavelengthDepdtVariable(fileName=fileName + '.dat')
    assert (mySpectrum.compare(mySpectrum3, rtol=1e-5))

    ##  check convert_to : f lambda dlambda ==> f nu dnu
    mySpectrum.convert_to(data_unit=u.milliJansky, wave_unit=u.micron, is_flux=True)
    assert (mySpectrum.data.unit == u.milliJansky)
    assert (mySpectrum.wavelength.unit == u.micron)

    #  http://www.stsci.edu/cgi-bin/convertcgl2
    #  5 microns ==> 33 Jy, 6 microns ==> 40 jy
    wavelength = (np.arange(2) + 5.) * 1E4 * u.AA
    flux = np.array((1, 1)) * (u.photon / u.cm ** 2 / u.s / u.AA)
    ref_flux = np.array([33., 40.]) * 1E3  # milliJansky
    mySpectrum = WavelengthDepdtVariable(wavelength=wavelength, data=flux)
    mySpectrum.convert_to(data_unit=u.milliJansky, wave_unit=u.micron, is_flux=True)
    # now mJy and wavelength in micron
    npt.assert_allclose(mySpectrum.data.value, ref_flux, rtol=1e-2)
    npt.assert_allclose(mySpectrum.wavelength.value, (np.arange(2) + 5.), rtol=1e-15)

    # Compare default conversion (is_flux = False)
    mySpectrum.convert_to(data_unit=u.microJansky, wave_unit=u.micron, is_flux=True)
    npt.assert_allclose(mySpectrum.data.value, ref_flux*1000, rtol=1e-2)
    # npt.assert_equal(mySpectrum.wavelength.data, (np.arange(2)+5.))

    os.remove(fileName + '.fits')
    os.remove(fileName + '.dat')
    return
