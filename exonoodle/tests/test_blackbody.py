#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                            TRANSIT GENERATOR                             #
#                      TEST FUNCION - SEDfileCreation                      #
# --------------------------------------------------------------------------#
#
# CONTRUBUTORS :
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#
# Part of TRANSIT GENERATOR program / Test for ToolBox
# Shall be in ./tests
#
#   to launch type >>> py.test test_blackbody.py


# External library imports
from astropy import units as u
from astropy.constants import R_sun
import numpy.testing as npt
import numpy as np

from astropy.modeling import models
import math

# Built-in library imports
from exonoodle.utils import blackBody


def test_blackbody():
    """
    Test Blackbody

    :return:
    :rtype:
    """
    status = True
    wave_tested = np.array([5.E-6]) * u.meter
    distance = 80. * u.parsec.to(u.meter) * u.meter
    result = blackBody(wave_tested, 4400. * u.Kelvin, 0.598 * R_sun, distance)
    result = result.to(u.Jy)

    # does not work for decimal 7  nrao
    reference = compute_flux(5., 4400., 80., 0.598)

    npt.assert_almost_equal(result.value[0], reference, decimal=6)

    return


def compute_flux(wave, temperature, distance, rstar):
    """
    Compute tmp
    Parameters
    ----------
    wave: wavelength in micron
    temperature: star temperature in Kelvin
    distance: star distance in parsec
    rstar: star radius in sun radius

    Returns
    -------
    tmp: star tmp on earth in Jansky

    Examples
    --------
    >>> compute_flux(5., 4400., 80., 0.598)
    0.030719686668305043 Jy

    >>> compute_flux(30000.0, 5800., math.pi/648000.,1)*1e-6
         1.24 megaJansky  sun

    >>> compute_flux(10., 5980., 10., 1.)
         2.38241486138    sun at 10 parsec
    """
    bb = models.BlackBody(temperature * u.K)
    flux_theoric_nu = bb(wave * u.um)

    # solid angle  in steradian
    # parsec= 648000./math.pi # in astronomical unit
    parsec = u.parsec.to(u.astronomical_unit)
    angle = np.arcsin(R_sun.value / u.astronomical_unit.to(u.meter))
    solid_angle = math.pi * (angle / distance / parsec * rstar) ** 2 * u.steradian
    flux = (flux_theoric_nu * solid_angle).to(u.jansky)

    return flux.value
