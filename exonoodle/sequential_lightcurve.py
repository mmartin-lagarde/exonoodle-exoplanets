#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
usefull source: https://stackoverflow.com/questions/5442910/python-multiprocessing-pool-map-for-multiple-arguments
"""

import numpy as np
from astropy import units as u
import os
import time
import shutil
from . import compute
from . import utils
from .version import __version__
from . import config

import logging
LOG = logging.getLogger('exonoodle.sequential_lightcurve')

def sequential_lightcurve(params):
    """

    :param params:
    :type params:
    :param time:
    :type time: float
    :return:
    :rtype:
    """

    utils.init_log(log="exonoodle.log", stdout_loglevel="INFO", file_loglevel="INFO")

    LOG.info("--------------------------------------------------------------------------------------------------------------------")
    LOG.info("                                                  -   ExoNoodle   -")
    LOG.info("                                              SEQUENTIAL LIGHTCURVE RUN")
    LOG.info("--------------------------------------------------------------------------------------------------------------------")
    LOG.info(" Version {} - CEA-Saclay\n".format(__version__))
    start_time = time.time()

    if isinstance(params, str):
        configuration = config.read(params)
    else:
        configuration = params

    folder = configuration["exonoodle"]["folder"]
    phase_min = configuration["exonoodle"]["phase_min"]
    phase_max = configuration["exonoodle"]["phase_max"]
    time_min = (2 * np.pi) * phase_min
    time_max = (2 * np.pi) * phase_max

    output_dir = utils.create_directory(folder)
    LOG.info('>> Directory created : {}/'.format(output_dir))

    #Creation of the time vector
    wavelength = config.init_waves(configuration)

    LOG.debug('   File: {}'.format(params))

    period = configuration["orbit"]["orbital_period"]
    sampling = configuration["exonoodle"]["time_sampling"]
    # If unit is time and not radians, do the conversion
    if sampling.decompose().unit == "s" :
        light_curve_sampling = utils.time2phase(sampling, period)
        light_curve_sampling = light_curve_sampling.value

    nb_points = int(np.ceil((time_max - time_min) / light_curve_sampling))
    times = np.linspace(time_min, time_max, nb_points) * u.rad  # radians
    times_in_seconds = (period / (2 * np.pi)) * times
    times_in_seconds -= times_in_seconds[0]  # First time in seconds is always 0
    padding = int(np.log10(nb_points)) + 1

    utils.times_file_creation(times, times_in_seconds, padding, output_dir)
    LOG.info('>> Time sheet created : {}/times.dat'.format(output_dir))

    LOG.info('>> Noodles start cooking')
    compute.phase_system_spectrum(times, configuration, output_dir, 0, padding, wavelength)
    LOG.info(' Done with success, files {}/SED_index.dat'.format(output_dir))

    end_time = time.time()

    output_filename = os.path.join(output_dir, os.path.basename(configuration.filename))
    shutil.copy(configuration.filename, output_filename)

    LOG.info("exoNoodle run: {:.2g} s".format(end_time - start_time))
    LOG.info("                                                                                    *** exoNoodle ***   End")
