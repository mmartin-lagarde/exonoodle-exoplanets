#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# --------------------------------------------------------------------------#
#                            TRANSIT GENERATOR                             #
#                    TEST FUNCION - geometryTransition                     #
# --------------------------------------------------------------------------#
#
# CONTRUBUTORS :
#   - (MML) : Marine Martin-Lagarde (CEA-Saclay)
#             marine.martin-lagarde@cea.fr
#   - (RG)  : René Gastaud (CEA-Saclay)
#             rene.gastaug@cea.fr
#
# Part of TRANSIT GENERATOR program / Test for ToolBox
# Shall be in ./tests


# External library imports
import numpy.testing as npt
import numpy as np

# Built-in library imports
from exonoodle.utils import geometryTransition


def test_geometryTransition(verbose=False):
    """
    Test Geometry Transition

    :param verbose:
    :type verbose:
    :return:
    :rtype:
    """
    status = True

    sizeRatio = np.asarray(0.15743)
    relativeDistance = np.asarray(1.)

    # relativeDistance = np.random.uniform(1-sizeRatio,1+sizeRatio)

    n = 100 * 100 * 100 * 10

    # Statistical funtion for reference (slow, less accurate)
    reference = statisticalGeometryTransition(relativeDistance, sizeRatio, plot=verbose, n=n)

    # Call the function to test
    value = geometryTransition(relativeDistance, sizeRatio)

    if verbose:
        print("sizeRatio={0:6.4F}, relativeDistance={1:6.4F}, reference={2:6.4F}, tmp={3:6.4F}".format(sizeRatio, relativeDistance, reference, value))

    npt.assert_almost_equal(reference, value, decimal=4)


def statisticalGeometryTransition(relativeDistance, sizeRatio, plot=False, n=1000):
    xy = np.random.uniform(-1, 1, 2 * n).reshape(2, n)

    #  points in the disk of the star
    r2 = xy[0] ** 2 + xy[1] ** 2
    ii = np.where(r2 <= 1)
    x = xy[0, ii].squeeze()
    y = xy[1, ii].squeeze()

    #  points in the disk of the star and of the planet
    distance_planet = (x - relativeDistance) ** 2 + y ** 2
    ip = np.where(distance_planet <= sizeRatio ** 2)
    xp = x[ip]
    yp = y[ip]

    #  calculus of the occulation
    star_surface = x.size
    occulted_surface = xp.size
    occultation_factor = 1 - float(occulted_surface) / float(star_surface)

    if plot:
        import matplotlib.pyplot as plt
        plt.figure(1)
        plt.title("Test of the star occultation factor")
        plt.axis('equal')
        plt.plot(xy[0], xy[1], 'g+')
        plt.plot(x, y, 'rx')
        plt.plot(xp, yp, 'bx')
        plt.show()

    return occultation_factor
